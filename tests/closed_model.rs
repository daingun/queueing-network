use reti_di_code::single_class::closed_model as cm;

#[test]
fn closed_model_terminal() {
    use cm::{ClosedModel, ServiceCentre};
    let sc1 = ServiceCentre::new_queueing_centre(1, 0.605_f32);
    let sc2 = ServiceCentre::new_queueing_centre(2, 2.1);
    let sc3 = ServiceCentre::new_queueing_centre(3, 1.35);
    let cs = ClosedModel::new(&[sc1, sc2, sc3], 15., 3_u16);
    let s = cs.solution().unwrap();

    assert_eq!("0.152", format!("{:.3}", s.throughput()));
    assert_eq!("4.80", format!("{:.2}", s.response_time()));
    assert_eq!("0.73", format!("{:.2}", s.queue()));

    assert_eq!("0.092", format!("{:.3}", s.centre_utilization(&1).unwrap()));
    assert_eq!("0.098", format!("{:.3}", s.centre_queue(&1).unwrap()));
    assert_eq!(
        "0.64",
        format!("{:.2}", s.centre_response_time(&1).unwrap())
    );
}

#[test]
fn closed_model_batch() {
    use cm::{ClosedModel, ServiceCentre};
    let sc1 = ServiceCentre::new_queueing_centre("1", 0.605_f32);
    let sc2 = ServiceCentre::new_queueing_centre("2", 2.1);
    let sc3 = ServiceCentre::new_queueing_centre("3", 1.35);
    let sc4 = ServiceCentre::new_delay_centre("4", 15.);
    let cs = ClosedModel::new(&[sc1, sc2, sc3, sc4], 0., 3_u16);
    let s = cs.solution().unwrap();

    assert_eq!("0.152", format!("{:.3}", s.throughput()));
    assert_eq!("19.80", format!("{:.2}", s.response_time()));
    assert_eq!("3.00", format!("{:.2}", s.queue()));

    assert_eq!(
        "0.092",
        format!("{:.3}", s.centre_utilization("1").unwrap())
    );
    assert_eq!("0.098", format!("{:.3}", s.centre_queue("1").unwrap()));
    assert_eq!(
        "0.64",
        format!("{:.2}", s.centre_response_time("1").unwrap())
    );
}

#[test]
fn approximate_closed_model_terminal() {
    use cm::{ClosedModel, ServiceCentre};
    let sc1 = ServiceCentre::new_queueing_centre(1, 0.605_f32);
    let sc2 = ServiceCentre::new_queueing_centre(2, 2.1);
    let sc3 = ServiceCentre::new_queueing_centre(3, 1.35);
    let cs = ClosedModel::new(&[sc1, sc2, sc3], 15., 3_u16);
    let s = cs.approximate_solution(0.001).unwrap();

    assert_eq!("0.1510", format!("{:.4}", s.throughput()));
    assert_eq!("4.8700", format!("{:.4}", s.response_time()));

    assert_eq!("0.0973", format!("{:.4}", s.centre_queue(&1).unwrap()));
    assert_eq!("0.4021", format!("{:.4}", s.centre_queue(&2).unwrap()));
    assert_eq!("0.2359", format!("{:.4}", s.centre_queue(&3).unwrap()));
}

#[test]
fn approximate_closed_model_batch() {
    use cm::{ClosedModel, ServiceCentre};
    let sc1 = ServiceCentre::new_queueing_centre(1, 0.605_f64);
    let sc2 = ServiceCentre::new_queueing_centre(2, 2.1);
    let sc3 = ServiceCentre::new_queueing_centre(3, 1.35);
    let sc4 = ServiceCentre::new_delay_centre(4, 15.);
    let cs = ClosedModel::new(&[sc1, sc2, sc3, sc4], 0., 3_u16);
    let s = cs.approximate_solution(0.001).unwrap();

    assert_eq!("0.1510", format!("{:.4}", s.throughput()));
    assert_eq!("19.8697", format!("{:.4}", s.response_time()));

    assert_eq!("0.0973", format!("{:.4}", s.centre_queue(&1).unwrap()));
    assert_eq!("0.4021", format!("{:.4}", s.centre_queue(&2).unwrap()));
    assert_eq!("0.2359", format!("{:.4}", s.centre_queue(&3).unwrap()));
    assert_eq!("2.2647", format!("{:.4}", s.centre_queue(&4).unwrap()));
}

#[test]
fn approximate_multiple_class_closed_model() {
    use reti_di_code::multiple_class::{
        closed_model::{ClosedModel, ServiceCentre},
        Classes,
    };
    let cpu = ServiceCentre::new_queueing_centre("CPU", &[("A", 1.0_f32), ("B", 2.)]);
    let disk = ServiceCentre::new_queueing_centre("DISK", &[("A", 3.), ("B", 4.)]);
    let centres = [cpu, disk];
    let class_a = "A";
    let class_b = "B";
    let mut think_time = Classes::new();
    think_time.insert(class_a, 0.);
    think_time.insert(class_b, 0.);

    let mut population = Classes::new();
    population.insert(class_a, 1_u32);
    population.insert(class_b, 1);

    let om = ClosedModel::new(&centres, think_time, population);
    let s = om.approximate_solution(0.001).unwrap();

    assert_eq!("0.154", format!("{:.3}", s.throughput(&class_a).unwrap()));
    assert_eq!("0.104", format!("{:.3}", s.throughput(&class_b).unwrap()));

    assert_eq!(
        "0.192",
        format!("{:.3}", s.centre_queue("CPU", class_a).unwrap())
    );
    assert_eq!(
        "0.248",
        format!("{:.3}", s.centre_queue("CPU", class_b).unwrap())
    );
    assert_eq!(
        "0.808",
        format!("{:.3}", s.centre_queue("DISK", class_a).unwrap())
    );
    assert_eq!(
        "0.752",
        format!("{:.3}", s.centre_queue("DISK", class_b).unwrap())
    );

    assert_eq!(
        "6.503",
        format!("{:.3}", s.response_time(&class_a).unwrap())
    );
    assert_eq!(
        "9.614",
        format!("{:.3}", s.response_time(&class_b).unwrap())
    );
}
