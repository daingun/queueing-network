#![allow(clippy::similar_names)]

#[test]
fn transaction_asymptotic_bounds() {
    use reti_di_code::bounds::Transaction;
    let t = Transaction::new(&[0.02_f32, 0.07, 0.04, 0.054, 0.03]).unwrap();
    let t_tab = t.throughput_asymptotic_bound();
    assert_eq!("0.00", format!("{:.2}", t_tab.lower()));
    assert_eq!("14.29", format!("{:.2}", t_tab.upper()));
    let t_rab = t.response_asymptotic_bound();
    assert_eq!("0.21", format!("{:.2}", t_rab.lower()));
    assert!(t_rab.upper().is_infinite());
}

#[test]
fn batch_asymptotic_bounds() {
    use reti_di_code::bounds::Batch;
    let b = Batch::new(&[5.0_f32, 4., 3.]).unwrap();
    let b_tab = b.throughput_asymptotic_bound(3.);
    assert_eq!("0.08", format!("{:.2}", b_tab.lower()));
    assert_eq!("0.20", format!("{:.2}", b_tab.upper()));
    let b_rab = b.response_asymptotic_bound(3.);
    assert_eq!("15.0", format!("{:.1}", b_rab.lower()));
    assert_eq!("36.0", format!("{:.1}", b_rab.upper()));
}

#[test]
fn terminal_asymptotic_bounds() {
    use reti_di_code::bounds::Terminal;
    let tm = Terminal::new(&[5.0_f32, 4., 3.], 15.).unwrap();
    let tm_tab = tm.throughput_asymptotic_bound(16.);
    assert_eq!("0.08", format!("{:.2}", tm_tab.lower()));
    assert_eq!("0.20", format!("{:.2}", tm_tab.upper()));
    let tm_rab = tm.response_asymptotic_bound(4.);
    assert_eq!("12.0", format!("{:.1}", tm_rab.lower()));
    assert_eq!("48.0", format!("{:.1}", tm_rab.upper()));
}
