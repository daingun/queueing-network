use reti_di_code::single_class::open_model as om;

#[test]
fn open_model() {
    use om::{OpenModel, ServiceCentre};
    let sc1 = ServiceCentre::new_queueing_centre(1, 0.605_f32);
    let sc2 = ServiceCentre::new_queueing_centre(2, 2.1);
    let sc3 = ServiceCentre::new_queueing_centre(3, 1.35);

    let om = OpenModel::new(&[sc1, sc2, sc3]);
    let s = om.solution(0.3);

    assert_eq!("0.476", format!("{:.3}", s.processing_capacity()));
    assert_eq!("8.684", format!("{:.3}", s.response_time()));
    assert_eq!("2.605", format!("{:.3}", s.queue()));

    assert_eq!("0.182", format!("{:.3}", s.centre_utilization(&1).unwrap()));
    assert_eq!("0.222", format!("{:.3}", s.centre_queue(&1).unwrap()));
    assert_eq!(
        "0.739",
        format!("{:.3}", s.centre_response_time(&1).unwrap())
    );
}

#[test]
fn multiple_class_open_model() {
    use reti_di_code::multiple_class::{
        open_model::{OpenModel, ServiceCentre},
        Classes,
    };
    let cpu = ServiceCentre::new_queueing_centre("CPU", &[("A", 1.0_f32), ("B", 2.)]);
    let disk = ServiceCentre::new_queueing_centre("DISK", &[("A", 3.), ("B", 4.)]);
    let centres = [cpu, disk];
    let class_a = "A";
    let class_b = "B";
    let mut classes = Classes::new();
    classes.insert(class_a, 3. / 19.);
    classes.insert(class_b, 2. / 19.);

    let om = OpenModel::new(&centres, classes);
    let s = om.solution();

    assert!(s.processing_capacity());
    assert_eq!("0.158", format!("{:.3}", s.throughput(&class_a).unwrap()));
    assert_eq!("0.105", format!("{:.3}", s.throughput(&class_b).unwrap()));

    assert_eq!(
        "0.158",
        format!("{:.3}", s.centre_utilization("CPU", class_a).unwrap())
    );
    assert_eq!(
        "1.58",
        format!("{:.2}", s.centre_response_time("CPU", class_a).unwrap())
    );
    assert_eq!(
        "0.25",
        format!("{:.2}", s.centre_queue("CPU", class_a).unwrap())
    );

    assert_eq!(
        "30.08",
        format!("{:.2}", s.response_time(&class_a).unwrap())
    );
}
