//! # Queueing Networks Models
//!
//! _E. D. Lazowska, J. Zahorjan, G. S. Graham, K. C. Sevcik,
//! Quantitative System Performance, Prentice-Hall Inc., 1984_
//!
//! A network of queues is a collection of service centres, which represent
//! system resources, and customers, which represent users or transactions.
//!

pub mod bounds;
pub mod error;
pub mod multiple_class;
pub mod single_class;

pub(crate) mod laws {
    use std::ops::{Div, Sub};

    // /// Utilization law: U = X * S
    // fn utilization_law(throughput: f32, service_requirement: f32) -> f32 {
    //     throughput * service_requirement
    // }

    // /// Little's law: N = X * R
    // fn little_law(throughput: f32, residence_time: f32) -> f32 {
    //     throughput * residence_time
    // }

    /// Response time law: R = N / X - Z
    pub(crate) fn response_time_law<T>(number_of_requests: T, throughput: T, think_time: T) -> T
    where
        T: Div<Output = T> + Sub<Output = T>,
    {
        number_of_requests / throughput - think_time
    }
}

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
enum ServiceKind {
    /// Delay centre
    DelayCentre,
    /// Queueing centre
    QueueingCentre,
}

pub trait One {
    fn one() -> Self;
}

pub trait Zero {
    fn zero() -> Self;
}

pub trait Max {
    #[must_use]
    fn max(self, b: Self) -> Self;
}

pub trait Min {
    #[must_use]
    fn min(self, b: Self) -> Self;
}

pub trait Inf {
    fn infinity() -> Self;
}

macro_rules! float_impl {
    ($($a:ident)*) => ($(
        impl One for $a {
            fn one() -> Self {
                1.
            }
        }
        impl Zero for $a {
            fn zero() -> Self {
                0.
            }
        }
        impl Max for $a {
            #[inline]
            fn max(self, b: Self) -> Self {
                $a::max(self, b)
            }
        }
        impl Min for $a {
            #[inline]
            fn min(self, b: Self) -> Self {
                $a::min(self, b)
            }
        }
        impl Inf for $a {
            fn infinity() -> Self {
                $a::INFINITY
            }
        }
    )*);
}

float_impl! {f32 f64}

macro_rules! integer_impl {
    ($($a:ident)*) => ($(
        impl One for $a {
            fn one() -> Self {
                1
            }
        }
    )*);
}

integer_impl! {i8 i16 i32 i64 i128 isize u8 u16 u32 u64 u128 usize}

use std::ops::{Add, Div, Mul, Sub};
pub trait Ops:
    Add<Output = Self> + Div<Output = Self> + Mul<Output = Self> + Sized + Sub<Output = Self>
{
}

impl<T> Ops for T where
    T: Add<Output = Self> + Div<Output = Self> + Mul<Output = Self> + Sized + Sub<Output = Self>
{
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub struct TryFromIntError(pub(crate) ());

impl std::error::Error for TryFromIntError {}

impl std::fmt::Display for TryFromIntError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Number cannot be converted safely")
    }
}

/// Coversion trait from the given type
pub trait TryFromInt<U>: Sized {
    /// Perfrom the conversion from the given argument.
    ///
    /// # Errors
    ///
    /// Returns an error if the argument cannot be converted
    fn try_from(u: U) -> Result<Self, TryFromIntError>;
}

macro_rules! try_from_unsigned_integer {
    ($source:ty => $($target:ty),*) => {$(
        impl TryFromInt<$source> for $target {
            #[inline]
            fn try_from(u: $source) -> Result<Self, TryFromIntError>
            {
                if u == 0 { return Ok(0.); }
                // Safe cast $source is either 4 or 8 bytes.
                let size: u32 = std::mem::size_of::<$source>() as u32 * 8;
                let lz = u.leading_zeros();
                let tz = u.trailing_zeros();
                // Leading bit is not used for sign in unsigned type.
                // Since the first one of the mantissa is implicit the actual
                // number of representable bits is #mantissa + 1.
                // Interger types cannot be converted in subnormal floats.
                if size - (lz + tz) > <$target>::MANTISSA_DIGITS + 1 {
                    Err(TryFromIntError(()))
                } else {
                    Ok(u as $target)
                }
            }
        }
    )*}
}

try_from_unsigned_integer!(usize => f32, f64);
try_from_unsigned_integer!(u32 => f32);
try_from_unsigned_integer!(u64 => f32, f64);
try_from_unsigned_integer!(u128 => f32, f64);

macro_rules! try_from_signed_integer {
    ($source:ty => $($target:ty),*) => {$(
        impl TryFromInt<$source> for $target {
            #[inline]
            fn try_from(u: $source) -> Result<Self, TryFromIntError>
            {
                if u == 0 { return Ok(0.); }
                // Safe cast $source is either 4 or 8 bytes.
                let size: u32 = std::mem::size_of::<$source>() as u32 * 8;
                let p = if u < 0 {-u} else {u};
                // Leading bit (sign) is now always zero;
                let lz = p.leading_zeros();
                let tz = p.trailing_zeros();
                // Since the first one of the mantissa is implicit the actual
                // number of representable bits is #mantissa + 1.
                // Interger types cannot be converted in subnormal floats.
                if size - (lz + tz) > <$target>::MANTISSA_DIGITS + 1 {
                    Err(TryFromIntError(()))
                } else {
                    Ok(u as $target)
                }
            }
        }
    )*}
}

try_from_signed_integer!(isize => f32, f64);
try_from_signed_integer!(i32 => f32);
try_from_signed_integer!(i64 => f32, f64);
try_from_signed_integer!(i128 => f32, f64);

macro_rules! from_safe_integer {
    ($source:ty => $($target:ty),*) => {$(
        impl TryFromInt<$source> for $target {
            #[inline]
            fn try_from(u: $source) -> Result<Self, TryFromIntError>
            {
                Ok(<$target>::from(u))
            }
        }
    )*}
}

from_safe_integer!(u8 => f32, f64);
from_safe_integer!(u16 => f32, f64);
from_safe_integer!(u32 => f64);
from_safe_integer!(i8 => f32, f64);
from_safe_integer!(i16 => f32, f64);
from_safe_integer!(i32 => f64);

#[cfg(test)]
mod tests {
    #![allow(clippy::float_cmp)]
    use super::*;

    #[test]
    fn response_time() {
        let r = laws::response_time_law(12., 3., 2.);
        assert_eq!(2., r);
    }

    #[test]
    fn trait_one() {
        assert_eq!(1., One::one());
    }

    #[test]
    fn trait_zero() {
        assert_eq!(0.0_f64, Zero::zero());
    }

    #[test]
    fn trait_max() {
        assert_eq!(13.94, 4.9.max(13.94));
    }

    #[test]
    fn trait_min() {
        assert_eq!(-31.74, 4.9_f64.min(-31.74));
    }

    #[test]
    fn trait_infinity() {
        assert!(f32::infinity().is_infinite());
    }

    #[test]
    fn int_one() {
        assert_eq!(1, i16::one());
    }

    #[test]
    fn error_display() {
        assert_eq!(
            "Number cannot be converted safely",
            TryFromIntError(()).to_string()
        );
    }

    #[test]
    fn try_from_unsigned() {
        assert_eq!(3., f32::try_from(3_usize).unwrap());
        assert_eq!(3., f64::try_from(3_usize).unwrap());
        assert_eq!(3., f32::try_from(3_u32).unwrap());
        assert_eq!(3., f32::try_from(3_u64).unwrap());
        assert_eq!(3., f64::try_from(3_u64).unwrap());
        assert_eq!(3., f32::try_from(3_u128).unwrap());
        assert_eq!(3., f64::try_from(3_u128).unwrap());
        assert!(f32::try_from(u32::MAX).is_err());
    }

    #[test]
    fn try_from_signed() {
        assert_eq!(3., f32::try_from(3_isize).unwrap());
        assert_eq!(3., f64::try_from(3_isize).unwrap());
        assert_eq!(3., f32::try_from(3_i32).unwrap());
        assert_eq!(3., f32::try_from(3_i64).unwrap());
        assert_eq!(3., f64::try_from(3_i64).unwrap());
        assert_eq!(3., f32::try_from(3_i128).unwrap());
        assert_eq!(3., f64::try_from(3_i128).unwrap());
        assert!(f32::try_from(i32::MAX).is_err());
    }

    #[test]
    fn try_from_safe() {
        assert_eq!(3., f32::try_from(3_u8).unwrap());
        assert_eq!(3., f64::try_from(3_u8).unwrap());
        assert_eq!(3., f32::try_from(3_u16).unwrap());
        assert_eq!(3., f64::try_from(3_u16).unwrap());
        assert_eq!(3., f64::try_from(3_u32).unwrap());

        assert_eq!(3., f32::try_from(3_i8).unwrap());
        assert_eq!(3., f64::try_from(3_i8).unwrap());
        assert_eq!(3., f32::try_from(3_i16).unwrap());
        assert_eq!(3., f64::try_from(3_i16).unwrap());
        assert_eq!(3., f64::try_from(3_i32).unwrap());
    }
}
