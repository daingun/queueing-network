//! # Models with Multiple Job classes

pub mod closed_model;
pub mod open_model;

use std::{borrow::Borrow, collections::HashMap, hash::Hash};

#[derive(Clone, Debug)]
pub struct Classes<K, V> {
    classes: HashMap<K, V>,
}

impl<K, V> Classes<K, V>
where
    K: Eq + Hash,
{
    /// Create a new empty class structure
    #[must_use]
    pub fn new() -> Self {
        Self {
            classes: HashMap::new(),
        }
    }

    /// Insert a new value in the class
    pub fn insert(&mut self, k: K, v: V) -> Option<V> {
        self.classes.insert(k, v)
    }

    /// Returns a reference to the value corresponding to the key
    fn get<Q: ?Sized>(&self, k: &Q) -> Option<&V>
    where
        K: Borrow<Q>,
        Q: Hash + Eq,
    {
        self.classes.get(k)
    }
}

impl<K, V> Default for Classes<K, V>
where
    K: Eq + Hash,
{
    fn default() -> Self {
        Self::new()
    }
}
