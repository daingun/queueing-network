//! # Multiple Class Closed Model Solution
//!
//! _E. D. Lazowska, J. Zahorjan, G. S. Graham, K. C. Sevcik,
//! Quantitative System Performance, Prentice-Hall Inc., 1984_
//!
//! Closed models refer to terminal or batch workloads and the solution technique
//! is an extension of the single class mean value analysis (MVA) algorithm.
//!
//! The solution returns:
//! * the throughput of the system for the given class
//! * the response time of the system for the given class
//! * the average number in system for the given class
//! * the queue length at each centre for the given class
//! * the response time of each centre for the given class
//! * the utilization of each centre for the given class
//!
//! # Approximate solution technique
//!
//! In the approximate solution the instant queue length are extimated iteratively.
//! The estimates are obtained based on the average queue length at the service
//! centres with the full customer population.
//!
//! # Examples
//!
//! ```
//! use reti_di_code::multiple_class::{closed_model::{ClosedModel, ServiceCentre}, Classes};
//! let cpu = ServiceCentre::new_queueing_centre("CPU", &[("A", 1.0_f32), ("B", 2.)]);
//! let disk = ServiceCentre::new_queueing_centre("DISK", &[("A", 3.), ("B", 4.)]);
//! let centres = [cpu, disk];
//! let class_a = "A";
//! let class_b = "B";
//! let mut think_time = Classes::new();
//! think_time.insert(class_a, 0.);
//! think_time.insert(class_b, 0.);
//!
//! let mut population = Classes::new();
//! population.insert(class_a, 1_u32);
//! population.insert(class_b, 1);
//!
//! let om = ClosedModel::new(&centres, think_time, population);
//! let s = om.approximate_solution(0.001).unwrap();
//!
//! // System output.
//! let t_a = s.throughput(&class_a).unwrap();
//! let t_b = s.throughput(&class_b).unwrap();
//! let r_a = s.response_time(&class_a).unwrap();
//! let r_b = s.response_time(&class_b).unwrap();
//! // Single centre output.
//! let c_q_a = s.centre_queue("CPU", class_a).unwrap();
//! let c_q_c = s.centre_queue("CPU", class_b).unwrap();
//! let d_q_a = s.centre_queue("DISK", class_a).unwrap();
//! let d_q_b = s.centre_queue("DISK", class_b).unwrap();
//! ```
//!

use std::{
    borrow::Borrow,
    collections::{
        hash_map::{Values, ValuesMut},
        HashMap,
    },
    hash::Hash,
    ops::{Neg, RangeInclusive},
};

use super::Classes;
use crate::{error::Error, One, Ops, ServiceKind, TryFromInt, Zero};

/// Struct holding the data of a single service centre
#[derive(Clone, Debug)]
pub struct ServiceCentre<I, C, T> {
    /// Identification
    id: I,
    /// Service centre kind
    kind: ServiceKind,
    /// Service centre data per class
    data: HashMap<C, CentreData<T>>,
}

impl<I, C, T> ServiceCentre<I, C, T>
where
    I: Clone,
{
    /// Get the identification of the service centre
    fn id(&self) -> I {
        self.id.clone()
    }
}

impl<I, C, T> ServiceCentre<I, C, T>
where
    C: Clone + Eq + Hash,
    T: Clone + Zero,
{
    /// Create a delay service centre
    ///
    /// # Arguments
    ///
    /// * `id` - Identification of the service centre
    /// * `demand` - Service centre demand
    pub fn new_delay_centre(id: I, demand: &[(C, T)]) -> Self {
        let mut data = HashMap::new();
        for (c, d) in demand {
            data.insert(c.clone(), CentreData::new(d.clone()));
        }
        ServiceCentre {
            id,
            kind: ServiceKind::DelayCentre,
            data,
        }
    }

    /// Create a queueing service centre
    ///
    /// # Arguments
    ///
    /// * `id` - Identification of the service centre
    /// * `demand` - Service centre demand
    pub fn new_queueing_centre(id: I, demand: &[(C, T)]) -> Self {
        let mut data = HashMap::new();
        for (c, d) in demand {
            data.insert(c.clone(), CentreData::new(d.clone()));
        }
        ServiceCentre {
            id,
            kind: ServiceKind::QueueingCentre,
            data,
        }
    }
}

/// Data of each service centre
#[derive(Clone, Debug)]
struct CentreData<T> {
    /// Service centre demand
    demand: T,
    /// Service centre utilization
    utilization: T,
    /// Service centre response time
    response_time: T,
    /// Service centre queue
    queue: T,
    /// Average queue length of the service centre
    average_queue_length: T,
}

impl<T> CentreData<T>
where
    T: Clone + Zero,
{
    /// Create a new `CentreData` given a `ServiceCentre` demand
    fn new(demand: T) -> Self {
        Self {
            demand,
            utilization: T::zero(),
            response_time: T::zero(),
            queue: T::zero(),
            average_queue_length: T::zero(),
        }
    }
}

impl<T> CentreData<T>
where
    T: Clone,
{
    /// Get the demand of the service centre
    fn demand(&self) -> T {
        self.demand.clone()
    }

    /// Get the utilization of the service centre
    #[allow(dead_code)]
    fn utilization(&self) -> T {
        self.utilization.clone()
    }

    /// Get the response time of the service centre
    #[allow(dead_code)]
    fn response_time(&self) -> T {
        self.response_time.clone()
    }

    /// Get the queue of the service centre
    fn queue(&self) -> T {
        self.queue.clone()
    }

    /// Get the queue of the service centre
    fn average_queue_length(&self) -> T {
        self.average_queue_length.clone()
    }
}

/// Collection key-value of the service centres
#[derive(Clone, Debug)]
struct System<K, C, V> {
    centres: HashMap<K, ServiceCentre<K, C, V>>,
}

impl<K, C, V> System<K, C, V> {
    /// Create a new system
    fn new() -> Self {
        Self {
            centres: HashMap::<K, ServiceCentre<K, C, V>>::new(),
        }
    }

    /// Returns the iterator over the data of the service centres.
    fn values(&self) -> Values<K, ServiceCentre<K, C, V>> {
        self.centres.values()
    }

    /// Returns the mutable iterator over the data of the service centres.
    fn values_mut(&mut self) -> ValuesMut<K, ServiceCentre<K, C, V>> {
        self.centres.values_mut()
    }
}

impl<K, C, V> System<K, C, V>
where
    K: Eq + Hash,
    C: Eq + Hash,
{
    /// Returns a reference to the value corresponding to the key
    fn get<Q: ?Sized, R: ?Sized>(&self, k: &Q, c: &R) -> Option<&CentreData<V>>
    where
        K: Borrow<Q>,
        Q: Eq + Hash,
        C: Borrow<R>,
        R: Eq + Hash,
    {
        self.centres.get(k).and_then(|sc| sc.data.get(c))
    }
}

impl<K, C, V> System<K, C, V>
where
    K: Eq + Hash + PartialEq,
{
    fn insert(&mut self, k: K, v: ServiceCentre<K, C, V>) -> Option<ServiceCentre<K, C, V>> {
        self.centres.insert(k, v)
    }
}

/// Input data for the close model
#[derive(Clone, Debug)]
pub struct ClosedModel<I, C, T, U> {
    /// Collection of service centres
    service_centres: System<I, C, T>,
    /// Think time of the system
    think_time: Classes<C, T>,
    /// Population of the system
    population: Classes<C, U>,
}

impl<I, C, T, U> ClosedModel<I, C, T, U>
where
    I: Clone + Eq + Hash,
    C: Clone + Eq + Hash,
    T: Clone + TryFromInt<U> + One + Ops + Zero,
    U: Clone + One,
    RangeInclusive<U>: IntoIterator<Item = U>,
{
    /// Create a new closed model
    ///
    /// # Arguments
    ///
    /// * `service_centres` - Collection of service centres
    /// * `think_time` - Think time of the terminals
    /// * `population` - Number of customers in the system
    pub fn new(
        service_centres: &[ServiceCentre<I, C, T>],
        think_time: Classes<C, T>,
        population: Classes<C, U>,
    ) -> Self {
        let mut system = System::new();
        for sc in service_centres {
            system.insert(sc.id(), sc.clone());
        }
        Self {
            service_centres: system,
            think_time,
            population,
        }
    }

    /// Compute the approximate solution for a closed model
    ///
    /// # Arguments
    ///
    /// * `abs` - absolute error to stop the iterations
    ///
    /// # Errors
    ///
    /// Returns an error if the population type cannot be converted into `T`,
    /// or the size of the service centres cannot be expressed as `T`
    #[allow(non_snake_case)]
    pub fn approximate_solution(&self, abs: T) -> Result<Solution<I, C, T>, Error>
    where
        T: Neg<Output = T> + PartialOrd + TryFromInt<usize>,
    {
        let mut new_pop = Classes::new();
        for (c, v) in &self.population.classes {
            let new_v = T::try_from(v.clone()).map_err(Error::conversion_error)?;
            new_pop.insert(c.clone(), new_v);
        }

        let mut cms = Solution {
            service_centres: self.service_centres.clone(),
            throughput: Classes::new(),
            think_time: self.think_time.clone(),
            population: new_pop,
        };

        Self::init_queue(&mut cms)?;
        Self::average_queue_length_hc(&mut cms)?;
        loop {
            Self::response_time(&mut cms);
            let R = Self::response_time_per_class(&cms);
            Self::throughput_per_class(&mut cms, &R)?;
            let tmp_queue = Self::new_queue(&cms)?;
            let stop = Self::stop_condition(&mut cms, &tmp_queue, abs.clone())?;
            if stop {
                break;
            }
            Self::average_queue_length_hc(&mut cms)?;
        }
        Ok(cms)
    }

    /// Initialize the queue length of each service center of the solution
    fn init_queue(solution: &mut Solution<I, C, T>) -> Result<(), Error>
    where
        T: TryFromInt<usize>,
    {
        let number_of_centres =
            T::try_from(solution.service_centres.centres.len()).map_err(Error::conversion_error)?;
        for sc in solution.service_centres.values_mut() {
            for (class, data) in &mut sc.data {
                let class_pop = solution
                    .population
                    .get(class)
                    .cloned()
                    .ok_or_else(Error::missing_class_error)?;
                data.queue = class_pop / number_of_centres.clone();
            }
        }
        Ok(())
    }

    /// Calculate the approximation of the custumers in queue at a centre
    #[allow(non_snake_case)]
    fn average_queue_length_hc(solution: &mut Solution<I, C, T>) -> Result<(), Error> {
        for sc in solution.service_centres.values_mut() {
            let mut sum_q = HashMap::<C, T>::new();
            for class in sc.data.keys() {
                sum_q.insert(
                    class.clone(),
                    sc.data
                        .iter()
                        .filter(|(c1, _)| &class != c1)
                        .fold(T::zero(), |acc, (_, data)| acc + data.queue()),
                );
            }
            for (class, data) in &mut sc.data {
                let Nc = solution
                    .population
                    .get(class)
                    .ok_or_else(Error::missing_class_error)?;
                let sq = sum_q
                    .get(class)
                    .ok_or_else(Error::missing_class_error)?
                    .clone();
                let a = (Nc.clone() - T::one()) / Nc.clone() * data.queue() + sq;
                data.average_queue_length = a;
            }
        }
        Ok(())
    }

    /// Calculate the response time of each service centre
    #[allow(non_snake_case)]
    fn response_time(solution: &mut Solution<I, C, T>) {
        for sc in solution.service_centres.values_mut() {
            for data in sc.data.values_mut() {
                data.response_time = match sc.kind {
                    ServiceKind::DelayCentre => data.demand(),
                    ServiceKind::QueueingCentre => {
                        data.demand() * (T::one() + data.average_queue_length())
                    }
                };
            }
        }
    }

    /// Calculate the response time per class
    #[allow(non_snake_case)]
    fn response_time_per_class(solution: &Solution<I, C, T>) -> Classes<C, T> {
        let mut R = Classes::<C, T>::new();
        for centre in solution.service_centres.values() {
            for (class, data) in &centre.data {
                let r = R.classes.entry(class.clone()).or_insert_with(T::zero);
                *r = r.clone() + data.response_time.clone();
            }
        }
        R
    }

    /// Calculate the throughput of each class
    #[allow(non_snake_case)]
    fn throughput_per_class(
        solution: &mut Solution<I, C, T>,
        R: &Classes<C, T>,
    ) -> Result<(), Error> {
        for (class, response_time) in &R.classes {
            let Nc = solution
                .population
                .get(class)
                .cloned()
                .ok_or_else(Error::missing_class_error)?;
            let Zc = solution
                .think_time
                .get(class)
                .cloned()
                .ok_or_else(Error::missing_class_error)?;
            let Xc = Nc / (Zc + response_time.clone());
            solution.throughput.insert(class.clone(), Xc);
        }
        Ok(())
    }

    /// Generate the new queues for each centre and class
    fn new_queue(solution: &Solution<I, C, T>) -> Result<HashMap<I, HashMap<C, T>>, Error> {
        let mut tmp_queue = HashMap::<I, HashMap<C, T>>::new();
        for (id, centre) in &solution.service_centres.centres {
            let mut asd = HashMap::new();
            for (class, data) in &centre.data {
                let thr = solution
                    .throughput
                    .get(class)
                    .cloned()
                    .ok_or_else(Error::missing_class_error)?;
                asd.insert(class.clone(), thr * data.response_time.clone());
            }
            tmp_queue.insert(id.clone(), asd);
        }
        Ok(tmp_queue)
    }

    /// Determine the iteration stopping condition
    fn stop_condition(
        solution: &mut Solution<I, C, T>,
        tmp_queue: &HashMap<I, HashMap<C, T>>,
        abs: T,
    ) -> Result<bool, Error>
    where
        T: Neg<Output = T> + PartialOrd,
    {
        let mut stop = true;
        for (id, sc) in &mut solution.service_centres.centres {
            for (class, data) in &mut sc.data {
                let q = tmp_queue
                    .get(id)
                    .and_then(|x| x.get(class))
                    .ok_or_else(Error::missing_class_error)?;
                let diff = data.queue.clone() - q.clone();
                stop = stop && (diff < abs && -diff < abs);
                data.queue = q.clone();
            }
        }
        Ok(stop)
    }
}

/// Struct holding the solution of a closed model
#[derive(Clone, Debug)]
pub struct Solution<I, C, T> {
    /// Service centres of the system
    service_centres: System<I, C, T>,
    /// Throughput of the entire system
    throughput: Classes<C, T>,
    /// Think time of the system
    think_time: Classes<C, T>,
    /// Population of the system
    population: Classes<C, T>,
}

impl<I, C, T> Solution<I, C, T>
where
    I: Eq + Hash,
    C: Eq + Hash,
    T: Clone + Ops,
{
    /// Get the throughput of the entire system
    ///
    /// # Arguments
    ///
    /// * `class` - Class identifier
    pub fn throughput<R: ?Sized>(&self, class: &R) -> Option<T>
    where
        C: Borrow<R>,
        R: Hash + Eq,
    {
        self.throughput.get(class).cloned()
    }

    /// Get the response time of the entire system for the given class.
    ///
    /// # Arguments
    ///
    /// * `class` - Class identifier
    pub fn response_time<R: ?Sized>(&self, class: &R) -> Option<T>
    where
        C: Borrow<R>,
        R: Hash + Eq,
    {
        let pop = self.population.get(class).cloned()?;
        let throu = self.throughput.get(class).cloned()?;
        let think = self.think_time.get(class).cloned()?;
        Some(crate::laws::response_time_law(pop, throu, think))
    }

    /// Get the average number of requests in the entire system
    ///
    /// # Arguments
    ///
    /// * `class` - Class identifier
    pub fn queue<R: ?Sized>(&self, class: &R) -> Option<T>
    where
        C: Borrow<R>,
        R: Hash + Eq,
    {
        let pop = self.population.get(class).cloned()?;
        let throu = self.throughput.get(class).cloned()?;
        let think = self.think_time.get(class).cloned()?;
        Some(pop - throu * think)
    }

    /// Get the number of requests in queue at the given service centre and class
    ///
    /// # Arguments
    ///
    /// * `centre` - Service centre identifier
    /// * `class` - Class identifier
    pub fn centre_queue<Q: ?Sized, R: ?Sized>(&self, centre: &Q, class: &R) -> Option<T>
    where
        I: Borrow<Q>,
        Q: Hash + Eq,
        C: Borrow<R>,
        R: Hash + Eq,
    {
        self.service_centres
            .get(centre, class)
            .map(|sc| sc.queue.clone())
    }

    /// Get the response time of the given service centre and class
    ///
    /// # Arguments
    ///
    /// * `centre` - Service centre identifier
    /// * `class` - Class identifier
    pub fn centre_response_time<Q: ?Sized, R: ?Sized>(&self, centre: &Q, class: &R) -> Option<T>
    where
        I: Borrow<Q>,
        Q: Hash + Eq,
        C: Borrow<R>,
        R: Hash + Eq,
    {
        self.service_centres
            .get(centre, class)
            .map(|sc| sc.response_time.clone())
    }

    /// Get the utilization of the given service centre and class
    ///
    /// # Arguments
    ///
    /// * `centre` - Service centre identifier
    /// * `class` - Class identifier
    pub fn centre_utilization<Q: ?Sized, R: ?Sized>(&self, centre: &Q, class: &R) -> Option<T>
    where
        I: Borrow<Q>,
        Q: Hash + Eq,
        C: Borrow<R>,
        R: Hash + Eq,
    {
        let t = self.throughput.get(class).cloned()?;
        self.service_centres
            .get(centre, class)
            .map(|sc| t * sc.demand.clone())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn approximate_closed_model() {
        let cpu = ServiceCentre::new_queueing_centre("CPU", &[("A", 1.0_f32), ("B", 2.)]);
        let disk = ServiceCentre::new_queueing_centre("DISK", &[("A", 3.), ("B", 4.)]);
        let centres = [cpu, disk];
        let class_a = "A";
        let class_b = "B";
        let mut think_time = Classes::new();
        think_time.insert(class_a, 0.);
        think_time.insert(class_b, 0.);

        let mut population = Classes::new();
        population.insert(class_a, 1_u32);
        population.insert(class_b, 1);

        let om = ClosedModel::new(&centres, think_time, population);
        let s = om.approximate_solution(0.001).unwrap();

        assert_eq!("0.154", format!("{:.3}", s.throughput(class_a).unwrap()));
        assert_eq!("0.104", format!("{:.3}", s.throughput(class_b).unwrap()));

        assert_eq!("1.000", format!("{:.3}", s.queue(class_a).unwrap()));
        assert_eq!(
            "0.192",
            format!("{:.3}", s.centre_queue("CPU", class_a).unwrap())
        );
        assert_eq!(
            "0.248",
            format!("{:.3}", s.centre_queue("CPU", class_b).unwrap())
        );
        assert_eq!(
            "0.808",
            format!("{:.3}", s.centre_queue("DISK", class_a).unwrap())
        );
        assert_eq!(
            "0.752",
            format!("{:.3}", s.centre_queue("DISK", class_b).unwrap())
        );

        assert_eq!(
            "6.503",
            format!("{:.3}", s.response_time(&class_a).unwrap())
        );
        assert_eq!(
            "9.614",
            format!("{:.3}", s.response_time(&class_b).unwrap())
        );
        assert_eq!(
            "2.386",
            format!("{:.3}", s.centre_response_time("CPU", class_b).unwrap())
        );
        assert_eq!(
            "7.228",
            format!("{:.3}", s.centre_response_time("DISK", class_b).unwrap())
        );
    }

    #[test]
    fn approximate_closed_model_with_delay() {
        let cpu = ServiceCentre::new_queueing_centre("CPU", &[("A", 1.0_f32), ("B", 2.)]);
        let disk = ServiceCentre::new_delay_centre("DISK", &[("A", 3.), ("B", 4.)]);
        let centres = [cpu, disk];
        let class_a = "A";
        let class_b = "B";
        let mut think_time = Classes::new();
        think_time.insert(class_a, 0.);
        think_time.insert(class_b, 0.);

        let mut population = Classes::new();
        population.insert(class_a, 1_u32);
        population.insert(class_b, 1);

        let om = ClosedModel::new(&centres, think_time, population);
        let s = om.approximate_solution(0.001).unwrap();

        assert_eq!("0.227", format!("{:.3}", s.throughput(class_a).unwrap()));
        assert_eq!("0.151", format!("{:.3}", s.throughput(class_b).unwrap()));

        assert_eq!(
            "0.318",
            format!("{:.3}", s.centre_queue("CPU", class_a).unwrap())
        );
        assert_eq!(
            "0.397",
            format!("{:.3}", s.centre_queue("CPU", class_b).unwrap())
        );
        assert_eq!(
            "0.682",
            format!("{:.3}", s.centre_queue("DISK", class_a).unwrap())
        );
        assert_eq!(
            "0.603",
            format!("{:.3}", s.centre_queue("DISK", class_b).unwrap())
        );

        assert_eq!("4.398", format!("{:.3}", s.response_time(class_a).unwrap()));
        assert_eq!("6.636", format!("{:.3}", s.response_time(class_b).unwrap()));

        assert_eq!(
            "0.301",
            format!("{:.3}", s.centre_utilization("CPU", class_b).unwrap())
        );
    }
}
