//! # Multiple Class Open Model Solution
//!
//! _E. D. Lazowska, J. Zahorjan, G. S. Graham, K. C. Sevcik,
//! Quantitative System Performance, Prentice-Hall Inc., 1984_
//!
//! Given `C` classes in the model, each class `c` has the arrival rate `λ`.
//!
//! The performance measures of interest are:
//!
//! * processing capacity (is the system able to handle the given load?)
//! * utilization per centre and class
//! * residence time per centre and class
//! * queue length per centre and class
//! * system response time per class
//! * average number in system per class
//! * throughput of system per class
//!
//! # Examples
//!
//! ```
//! use reti_di_code::multiple_class::{open_model::{OpenModel, ServiceCentre}, Classes};
//! let cpu = ServiceCentre::new_queueing_centre("CPU", &[("A", 1.0_f32), ("B", 2.)]);
//! let disk = ServiceCentre::new_queueing_centre("DISK", &[("A", 3.), ("B", 4.)]);
//! let centres = [cpu, disk];
//! let class_a = "A";
//! let class_b = "B";
//! let mut classes = Classes::new();
//! classes.insert(class_a, 3. / 19.);
//! classes.insert(class_b, 2. / 19.);
//!
//! let om = OpenModel::new(&centres, classes);
//! let s = om.solution();
//!
//! // System output.
//! assert_eq!(true, s.processing_capacity());
//! let t = s.throughput(&class_a).unwrap();
//! let r = s.response_time(&class_a).unwrap();
//! let q = s.queue(&class_a).unwrap();
//! // Single centre output.
//! let c_q = s.centre_queue("CPU", class_a).unwrap();
//! let c_r_t = s.centre_response_time("CPU", class_a).unwrap();
//! let c_u = s.centre_utilization("CPU", class_a).unwrap();
//! ```
//!

use std::{
    borrow::Borrow,
    collections::{
        hash_map::{Values, ValuesMut},
        HashMap,
    },
    hash::Hash,
};

use super::Classes;
use crate::{One, Ops, ServiceKind, Zero};

/// Types of service centres
#[derive(Clone, Debug)]
pub struct ServiceCentre<I, C, T> {
    /// Identification
    id: I,
    /// Kind
    kind: ServiceKind,
    /// Service centre data per class
    data: HashMap<C, CentreData<T>>,
}

impl<I, C, T> ServiceCentre<I, C, T>
where
    I: Clone,
{
    /// Get the identification of the service centre
    fn id(&self) -> I {
        self.id.clone()
    }
}

impl<I, C, T> ServiceCentre<I, C, T>
where
    C: Clone + Eq + Hash,
    T: Clone + Zero,
{
    /// Create a new delay service centre
    ///
    /// # Arguments
    ///
    /// * `id` - Identification of the service centre
    /// * `demand` - Demand of the service centre
    pub fn new_delay_centre(id: I, demand: &[(C, T)]) -> Self {
        let mut data = HashMap::new();
        for (c, d) in demand {
            data.insert(c.clone(), CentreData::new(d.clone()));
        }
        ServiceCentre {
            id,
            kind: ServiceKind::DelayCentre,
            data,
        }
    }

    /// Create a new queueing service centre
    ///
    /// # Arguments
    ///
    /// * `id` - Identification of the service centre
    /// * `demand` - Demand of the service centre
    pub fn new_queueing_centre(id: I, demand: &[(C, T)]) -> Self {
        let mut data = HashMap::new();
        for (c, d) in demand {
            data.insert(c.clone(), CentreData::new(d.clone()));
        }
        ServiceCentre {
            id,
            kind: ServiceKind::QueueingCentre,
            data,
        }
    }
}

/// Service centre data
#[derive(Clone, Debug)]
struct CentreData<T> {
    /// Service centre demand
    demand: T,
    /// Service centre utilization
    utilization: T,
    /// Service centre response time
    response_time: T,
    /// Service centre queue
    queue: T,
}

impl<T> CentreData<T>
where
    T: Clone + Zero,
{
    /// Create the `CentreData` struct
    ///
    /// # Arguments
    ///
    /// * `demand` - Service centre demand
    fn new(demand: T) -> Self {
        Self {
            demand,
            utilization: T::zero(),
            response_time: T::zero(),
            queue: T::zero(),
        }
    }
}

impl<T> CentreData<T>
where
    T: Clone,
{
    /// Get the demand of the service centre
    fn demand(&self) -> T {
        self.demand.clone()
    }

    /// Get the utilization of the service centre
    fn utilization(&self) -> T {
        self.utilization.clone()
    }

    /// Get the response time of the service centre
    fn response_time(&self) -> T {
        self.response_time.clone()
    }

    /// Get the queue of the service centre
    fn queue(&self) -> T {
        self.queue.clone()
    }
}

/// Collection key-value of the service centres
#[derive(Clone, Debug)]
struct System<K, C, V> {
    centres: HashMap<K, ServiceCentre<K, C, V>>,
}

impl<K, C, V> System<K, C, V> {
    /// Create a new system
    fn new() -> Self {
        Self {
            centres: HashMap::<K, ServiceCentre<K, C, V>>::new(),
        }
    }

    /// Returns the iterator over the data of the service centres.
    fn values(&self) -> Values<K, ServiceCentre<K, C, V>> {
        self.centres.values()
    }

    /// Returns the mutable iterator over the data of the service centres.
    fn values_mut(&mut self) -> ValuesMut<K, ServiceCentre<K, C, V>> {
        self.centres.values_mut()
    }
}

impl<K, C, V> System<K, C, V>
where
    K: Eq + Hash,
    C: Eq + Hash,
{
    /// Returns a reference to the value corresponding to the key
    fn get<Q: ?Sized, R: ?Sized>(&self, k: &Q, c: &R) -> Option<&CentreData<V>>
    where
        K: Borrow<Q>,
        Q: Eq + Hash,
        C: Borrow<R>,
        R: Eq + Hash,
    {
        self.centres.get(k).and_then(|sc| sc.data.get(c))
    }
}

impl<K, C, V> System<K, C, V>
where
    K: Eq + Hash + PartialEq,
{
    fn insert(&mut self, k: K, v: ServiceCentre<K, C, V>) -> Option<ServiceCentre<K, C, V>> {
        self.centres.insert(k, v)
    }
}

/// Open model
#[derive(Clone, Debug)]
pub struct OpenModel<I, C, T> {
    /// Service centre list
    service_centres: System<I, C, T>,
    /// Classes arrival rates
    arrival_rates: Classes<C, T>,
}

impl<I, C, T> OpenModel<I, C, T>
where
    I: Clone + Eq + Hash,
    C: Clone + Eq + Hash,
    T: Clone + One + Ops + PartialOrd + Zero,
{
    /// Create an open model system
    ///
    /// # Arguments
    ///
    /// * `service_centres` - Service centres
    /// * `classes` - Classes of the system
    pub fn new(service_centres: &[ServiceCentre<I, C, T>], arrival_rates: Classes<C, T>) -> Self {
        let mut system = System::new();
        for sc in service_centres {
            system.insert(sc.id(), sc.clone());
        }

        Self {
            service_centres: system,
            arrival_rates,
        }
    }

    /// Calculate the solution for the open model
    #[must_use]
    pub fn solution(&self) -> Solution<I, C, T> {
        let proc = self.sufficient_processing_capacity();

        let throughput = self.arrival_rates.clone();

        let mut service_centres = self.service_centres.clone();
        for sc in service_centres.values_mut() {
            self.utilization(sc);
            let utilization_per_centre = sc
                .data
                .values()
                .map(CentreData::utilization)
                .fold(T::zero(), |acc, u| acc + u);
            for data in sc.data.values_mut() {
                data.response_time =
                    response_time(sc.kind, data.demand(), utilization_per_centre.clone());
            }
            self.queue(sc);
        }

        let mut system_response_time = Classes::new();
        let mut average_number_in_system = Classes::new();
        for sc in service_centres.values() {
            for (c, data) in &sc.data {
                let r = system_response_time
                    .classes
                    .entry(c.clone())
                    .or_insert_with(T::zero);
                *r = r.clone() + data.response_time.clone();
                let q = average_number_in_system
                    .classes
                    .entry(c.clone())
                    .or_insert_with(T::zero);
                *q = q.clone() + data.queue.clone();
            }
        }

        Solution {
            processing_capacity: proc,
            throughput,
            service_centres,
            system_response_time,
            average_number_in_system,
        }
    }

    /// Determine if the system has the sufficient capacity to process
    /// the given load.
    #[allow(clippy::option_if_let_else)]
    fn sufficient_processing_capacity(&self) -> bool {
        self.service_centres.values().all(|sc| {
            let sum = sc.data.iter().fold(T::zero(), |acc, (class, data)| {
                if let Some(arrival_rate) = self.arrival_rates.get(class) {
                    acc + arrival_rate.clone() * data.demand()
                } else {
                    acc
                }
            });
            sum < T::one()
        })
    }

    /// Calculate the utilization given the class and the demand.
    ///
    /// # Arguments
    ///
    /// * `service_centre` - Service centre
    fn utilization(&self, service_centre: &mut ServiceCentre<I, C, T>) {
        for (c, data) in &mut service_centre.data {
            let ar = self.arrival_rates.get(c).map_or(T::zero(), Clone::clone);
            data.utilization = ar.clone() * data.demand();
        }
    }

    /// Calculate the queue length given the class and the response time.
    ///
    /// # Arguments
    ///
    /// * `service_centre` - Service centre
    fn queue(&self, service_centre: &mut ServiceCentre<I, C, T>) {
        for (c, data) in &mut service_centre.data {
            let ar = self.arrival_rates.get(c).map_or(T::zero(), Clone::clone);
            data.queue = ar.clone() * data.response_time.clone();
        }
    }
}

/// Calculate the response time givent the demand and the utilization
///
/// # Arguments
///
/// * `kind` - Kind of service centre
/// * `demand` - Service demand
/// * `utilization` - Service centre utilization
fn response_time<T>(kind: ServiceKind, demand: T, utilization: T) -> T
where
    T: One + Ops,
{
    match kind {
        ServiceKind::DelayCentre => demand,
        ServiceKind::QueueingCentre => demand / (T::one() - utilization),
    }
}

/// Structure holding data for the solution of an open model
#[derive(Clone, Debug)]
pub struct Solution<I: Eq + Hash, C, T> {
    /// Processing capacity of the entire system
    processing_capacity: bool,
    /// Throughput of the entire system
    throughput: Classes<C, T>,
    /// Service centres informations
    service_centres: System<I, C, T>,
    /// Response time of the entire system
    system_response_time: Classes<C, T>,
    /// Average number of customers in the system
    average_number_in_system: Classes<C, T>,
}

impl<I, C, T> Solution<I, C, T>
where
    I: Eq + Hash,
    C: Eq + Hash,
    T: Clone,
{
    /// Assert if the entire system can handle the load.
    #[must_use]
    pub fn processing_capacity(&self) -> bool {
        self.processing_capacity
    }

    /// Get the throughput of the entire system for the given class.
    ///
    /// # Arguments
    ///
    /// * `class` - Class identifier
    pub fn throughput<R: ?Sized>(&self, class: &R) -> Option<T>
    where
        C: Borrow<R>,
        R: Hash + Eq,
    {
        self.throughput.get(class).cloned()
    }

    /// Get the response time of the entire system for the given class.
    ///
    /// # Arguments
    ///
    /// * `class` - Class identifier
    pub fn response_time<R: ?Sized>(&self, class: &R) -> Option<T>
    where
        C: Borrow<R>,
        R: Hash + Eq,
    {
        self.system_response_time.get(class).cloned()
    }

    /// Get the average number in the entire system for the given class.
    ///
    /// # Arguments
    ///
    /// * `class` - Class identifier
    pub fn queue<R: ?Sized>(&self, class: &R) -> Option<T>
    where
        C: Borrow<R>,
        R: Hash + Eq,
    {
        self.average_number_in_system.get(class).cloned()
    }

    /// Get the number of requests in queue at the given service centre and class
    ///
    /// # Arguments
    ///
    /// * `centre` - Service centre identifier
    /// * `class` - Class identifier
    pub fn centre_queue<Q: ?Sized, R: ?Sized>(&self, centre: &Q, class: &R) -> Option<T>
    where
        I: Borrow<Q>,
        Q: Hash + Eq,
        C: Borrow<R>,
        R: Hash + Eq,
    {
        self.service_centres
            .get(centre, class)
            .map(CentreData::queue)
    }

    /// Get the response time of the given service centre and class
    ///
    /// # Arguments
    ///
    /// * `centre` - Service centre identifier
    /// * `class` - Class identifier
    pub fn centre_response_time<Q: ?Sized, R: ?Sized>(&self, centre: &Q, class: &R) -> Option<T>
    where
        I: Borrow<Q>,
        Q: Hash + Eq,
        C: Borrow<R>,
        R: Hash + Eq,
    {
        self.service_centres
            .get(centre, class)
            .map(CentreData::response_time)
    }

    /// Get the utilization of the given service centre and class
    ///
    /// # Arguments
    ///
    /// * `centre` - Service centre identifier
    /// * `class` - Class identifier
    pub fn centre_utilization<Q: ?Sized, R: ?Sized>(&self, centre: &Q, class: &R) -> Option<T>
    where
        I: Borrow<Q>,
        Q: Hash + Eq,
        C: Borrow<R>,
        R: Hash + Eq,
    {
        self.service_centres
            .get(centre, class)
            .map(CentreData::utilization)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn multiple_class_open_model() {
        let cpu = ServiceCentre::new_queueing_centre("CPU", &[("A", 1.0_f32), ("B", 2.)]);
        let disk = ServiceCentre::new_queueing_centre("DISK", &[("A", 3.), ("B", 4.)]);
        let centres = [cpu, disk];
        let class_a = "A";
        let class_b = "B";
        let mut classes = Classes::new();
        classes.insert(class_a, 3. / 19.); // jobs/second
        classes.insert(class_b, 2. / 19.); // jobs/second

        let om = OpenModel::new(&centres, classes);
        let s = om.solution();

        assert!(s.processing_capacity());
        assert_eq!("0.158", format!("{:.3}", s.throughput(class_a).unwrap()));
        assert_eq!("0.105", format!("{:.3}", s.throughput(class_b).unwrap()));

        assert_eq!(
            "0.158",
            format!("{:.3}", s.centre_utilization("CPU", class_a).unwrap())
        );
        assert_eq!(
            "1.58",
            format!("{:.2}", s.centre_response_time("CPU", class_a).unwrap())
        );
        assert_eq!(
            "0.25",
            format!("{:.2}", s.centre_queue("CPU", class_a).unwrap())
        );

        assert_eq!("30.08", format!("{:.2}", s.response_time(class_a).unwrap()));

        assert_eq!("4.75", format!("{:.2}", s.queue(class_a).unwrap()));
    }

    #[test]
    fn multiple_class_open_model_with_delay() {
        let cpu = ServiceCentre::new_queueing_centre("CPU", &[("A", 1.0_f32), ("B", 2.)]);
        let disk = ServiceCentre::new_delay_centre("DISK", &[("A", 3.), ("B", 4.)]);
        let centres = [cpu, disk];
        let class_a = "A";
        let class_b = "B";
        let mut classes = Classes::new();
        classes.insert(class_a, 3. / 19.);
        classes.insert(class_b, 2. / 19.);

        let om = OpenModel::new(&centres, classes);
        let s = om.solution();

        assert!(s.processing_capacity());
        assert_eq!("0.158", format!("{:.3}", s.throughput(class_a).unwrap()));
        assert_eq!("0.105", format!("{:.3}", s.throughput(class_b).unwrap()));

        assert_eq!(
            "0.158",
            format!("{:.3}", s.centre_utilization("CPU", class_a).unwrap())
        );
        assert_eq!(
            "1.58",
            format!("{:.2}", s.centre_response_time("CPU", class_a).unwrap())
        );
        assert_eq!(
            "0.25",
            format!("{:.2}", s.centre_queue("CPU", class_a).unwrap())
        );

        assert_eq!("4.58", format!("{:.2}", s.response_time(class_a).unwrap()));

        assert_eq!("0.72", format!("{:.2}", s.queue(class_a).unwrap()));
    }
}
