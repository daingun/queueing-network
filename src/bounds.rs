//! # Bounds on performance
//!
//! _E. D. Lazowska, J. Zahorjan, G. S. Graham, K. C. Sevcik,
//! Quantitative System Performance, Prentice-Hall Inc., 1984_
//!
//! Determine upper and lower bounds on system throughput and response time
//! as functions of the system workload intensity.
//!
//! Models parameters:
//! * `K`, the number of service centres;
//! * `Dmax`, the largest service demand at any sigle centre;
//! * `D`, the sum of the service demands at the centres;
//! * the `type` of the customer class (`Batch`, `Terminal`, `Transaction`);
//! * `Z`, the average thick time;
//!
//! The throughput upper and the response time lower bounds are optimistic.
//!
//! The throughput lower and the response time upper bounds are pessimistic.
//!
//! ## Asymptotic bounds
//!
//! For `Transaction` workloads, the throughput bound indicates the maximum
//! possible arrival rate of customers that the system can process successfully.
//! Response time bounds indicate the largest and smallest possible response
//! times experienced by customers when the system has a given arrival rate.
//!
//! For `Batch` and `Terminal` workloads the smallest possible throughput
//! occurs when each additional customer is forced to queue behind all
//! other customers already in the system. The largest possible throughput
//! occurs when each additional customer is not delayed at all by any other
//! customers in the system. Response time bounds can be deduced from throughput
//! using Little's law.
//!
//! ## Balanced system bounds
//!
//! Balance system bounds are based upon systems that are "balanced" in the
//! sense that the service demand at every centre is the same.
//!
//! # Example
//!
//! ```
//! use reti_di_code::bounds::{Batch, Terminal, Transaction};
//! let transaction = Transaction::new(&[1.0, 2.0, 3.0]).unwrap();
//! let batch = Batch::new(&[1.0, 2.0, 3.0]).unwrap();
//! let terminal = Terminal::new(&[1.0, 2.0, 3.0], 15.).unwrap();
//! let bounds = transaction.throughput_asymptotic_bound();
//! let lower = bounds.lower();
//! let upper = bounds.upper();
//! assert_eq!(0., lower);
//! assert!((0.333_f32 - upper).abs() < 0.001);
//! ```

use std::ops::Add;

use crate::{error::Error, Inf, Max, Min, One, Ops, TryFromInt, Zero};

/// Calculate the largest service demands given a list
///
/// # Arguments
///
/// * `demands` - Slice of service centre demands
fn largest_service_demand<'a, T, I>(demands: I) -> T
where
    T: 'a + Clone + PartialOrd,
    I: IntoIterator<Item = &'a T>,
{
    demands
        .into_iter()
        .max_by(|x, y| x.partial_cmp(y).unwrap())
        .unwrap()
        .clone()
}

/// Calculate the sum of service demands given a list
///
/// # Arguments
///
/// * `demands` - Slice of service centre demands
fn sum_of_service_demands<'a, T, I>(demands: I) -> T
where
    T: 'a + Add<Output = T> + Clone + Zero,
    I: IntoIterator<Item = &'a T>,
{
    demands
        .into_iter()
        .fold(T::zero(), |acc, x| acc + x.clone())
}

/// Performance bounds
pub struct Bounds<T> {
    /// Lower bound
    lower: T,
    /// Upper bound
    upper: T,
}

impl<T> Bounds<T>
where
    T: Clone,
{
    /// Get the lower bound
    pub fn lower(&self) -> T {
        self.lower.clone()
    }

    /// Get the higher bound
    pub fn upper(&self) -> T {
        self.upper.clone()
    }
}

impl<T> From<(T, T)> for Bounds<T> {
    fn from(from: (T, T)) -> Self {
        Bounds {
            lower: from.0,
            upper: from.1,
        }
    }
}

/// System of transaction type
pub struct Transaction<T> {
    /// Largest service demand of service centres
    largest_service_demand: T,
    /// Sum of service demands of service centres
    sum_of_service_demands: T,
    /// Average service demand of service centres
    average_service_demand: T,
}

impl<T> Transaction<T>
where
    T: Clone + Inf + One + Ops + PartialOrd + Zero,
{
    /// Create a new `Transaction` system
    ///
    /// # Arguments
    ///
    /// * `demands` - Service centres demands
    ///
    /// # Errors
    ///
    /// Returns an error if the size of the argument cannot be converted to `T`
    pub fn new(demands: &[T]) -> Result<Self, Error>
    where
        T: TryFromInt<usize>,
    {
        let sum_of_service_demands = sum_of_service_demands(demands);
        let length = T::try_from(demands.len()).map_err(Error::conversion_error)?;
        Ok(Self {
            largest_service_demand: largest_service_demand(demands),
            sum_of_service_demands: sum_of_service_demands.clone(),
            average_service_demand: sum_of_service_demands / length,
        })
    }

    /// Calculate system asymptotic bound for throughput
    pub fn throughput_asymptotic_bound(&self) -> Bounds<T> {
        (T::zero(), T::one() / self.largest_service_demand.clone()).into()
    }

    /// Calculate system asymptotic bound for response time
    pub fn response_asymptotic_bound(&self) -> Bounds<T> {
        (self.sum_of_service_demands.clone(), T::infinity()).into()
    }

    /// Calculate balanced system bound for throughput
    pub fn throughput_balanced_system_bound(&self) -> Bounds<T> {
        (T::zero(), T::one() / self.largest_service_demand.clone()).into()
    }

    /// Calculate balanced system bound for response time
    ///
    /// # Arguments
    ///
    /// `arrival_rate` - Arrival rate for transaction
    #[allow(non_snake_case)]
    pub fn response_balanced_system_bound(&self, arrival_rate: T) -> Bounds<T> {
        let D = self.sum_of_service_demands.clone();
        let Dmax = self.largest_service_demand.clone();
        let Dave = self.average_service_demand.clone();
        let lambda = arrival_rate;
        let lower = D.clone() / (T::one() - lambda.clone() * Dave);
        let upper = D / (T::one() - lambda * Dmax);
        (lower, upper).into()
    }
}

/// System of batch type
pub struct Batch<T> {
    /// Largest service demand of service centres
    largest_service_demand: T,
    /// Sum of service demands of service centres
    sum_of_service_demands: T,
    /// Average service demand of service centres
    average_service_demand: T,
}

impl<T> Batch<T>
where
    T: Clone + Max + Min + One + Ops + PartialOrd + Zero,
{
    /// Create a new `Batch` system
    ///
    /// # Arguments
    ///
    /// * `demands` - Service centres demands
    ///
    /// # Errors
    ///
    /// Returns an error if the size of the argument cannot be converted to `T`
    pub fn new(demands: &[T]) -> Result<Self, Error>
    where
        T: TryFromInt<usize>,
    {
        let sum_of_service_demands = sum_of_service_demands(demands);
        let length = T::try_from(demands.len()).map_err(Error::conversion_error)?;
        Ok(Self {
            largest_service_demand: largest_service_demand(demands),
            sum_of_service_demands: sum_of_service_demands.clone(),
            average_service_demand: sum_of_service_demands / length,
        })
    }

    /// Calculate system asymptotic bound for throughput
    ///
    /// # Arguments
    ///
    /// `requests` - Number of requests
    #[allow(non_snake_case)]
    pub fn throughput_asymptotic_bound(&self, number_of_requests: T) -> Bounds<T> {
        let N = number_of_requests;
        let D = self.sum_of_service_demands.clone();
        let Dmax = self.largest_service_demand.clone();
        let lower = T::one() / D.clone();
        let upper = T::min(N / D, T::one() / Dmax);
        (lower, upper).into()
    }

    /// Calculate system asymptotic bound for response time
    ///
    /// # Arguments
    ///
    /// `requests` - Number of requests
    #[allow(non_snake_case)]
    pub fn response_asymptotic_bound(&self, number_of_requests: T) -> Bounds<T> {
        let N = number_of_requests;
        let D = self.sum_of_service_demands.clone();
        let Dmax = self.largest_service_demand.clone();
        let lower = T::max(D.clone(), N.clone() * Dmax);
        let upper = N * D;
        (lower, upper).into()
    }

    /// Calculate balanced system bound for throughput
    ///
    /// # Arguments
    ///
    /// `requests` - Number of requests
    #[allow(non_snake_case)]
    pub fn throughput_balanced_system_bound(&self, number_of_requests: T) -> Bounds<T> {
        let N = number_of_requests;
        let D = self.sum_of_service_demands.clone();
        let Dmax = self.largest_service_demand.clone();
        let Dave = self.average_service_demand.clone();
        let lower = N.clone() / (D.clone() + (N.clone() - T::one()) * Dmax.clone());
        let upper = T::min(T::one() / Dmax, N.clone() / (D + (N - T::one()) * Dave));
        (lower, upper).into()
    }

    /// Calculate balanced system bound for response time
    ///
    /// # Arguments
    ///
    /// `requests` - Number of requests
    #[allow(non_snake_case)]
    pub fn response_balanced_system_bound(&self, number_of_requests: T) -> Bounds<T> {
        let N = number_of_requests;
        let D = self.sum_of_service_demands.clone();
        let Dmax = self.largest_service_demand.clone();
        let Dave = self.average_service_demand.clone();
        let lower = T::max(
            N.clone() * Dmax.clone(),
            D.clone() + (N.clone() - T::one()) * Dave,
        );
        let upper = D + (N - T::one()) * Dmax;
        (lower, upper).into()
    }
}

/// System of terminal type
pub struct Terminal<T> {
    /// Think time of terminals
    think_time: T,
    /// Largest service demand of service centres
    largest_service_demand: T,
    /// Sum of service demands of service centres
    sum_of_service_demands: T,
    /// Average service demand of service centres
    average_service_demand: T,
}

impl<T> Terminal<T>
where
    T: Clone + Max + Min + One + Ops + PartialOrd + Zero,
{
    /// Create a new `Terminal` system
    ///
    /// # Arguments
    ///
    /// * `demands` - Service centres demands
    /// * `think_time` - Think time of terminals
    ///
    /// # Errors
    ///
    /// Returns an error if the size of the argument cannot be converted to `T`
    pub fn new(demands: &[T], think_time: T) -> Result<Self, Error>
    where
        T: TryFromInt<usize>,
    {
        let sum_of_service_demands = sum_of_service_demands(demands);
        let length = T::try_from(demands.len()).map_err(Error::conversion_error)?;
        Ok(Self {
            think_time,
            largest_service_demand: largest_service_demand(demands),
            sum_of_service_demands: sum_of_service_demands.clone(),
            average_service_demand: sum_of_service_demands / length,
        })
    }

    /// Calculate system asymptotic bound for throughput
    ///
    /// # Arguments
    ///
    /// `requests` - Number of requests
    #[allow(non_snake_case)]
    pub fn throughput_asymptotic_bound(&self, requests: T) -> Bounds<T> {
        let N = requests;
        let D = self.sum_of_service_demands.clone();
        let Dmax = self.largest_service_demand.clone();
        let Z = self.think_time.clone();
        let lower = N.clone() / (N.clone() * D.clone() + Z.clone());
        let upper = T::min(N / (D + Z), T::one() / Dmax);
        (lower, upper).into()
    }

    /// Calculate system asymptotic bound for response time
    ///
    /// # Arguments
    ///
    /// `requests` - Number of requests
    #[allow(non_snake_case)]
    pub fn response_asymptotic_bound(&self, requests: T) -> Bounds<T> {
        let N = requests;
        let D = self.sum_of_service_demands.clone();
        let Dmax = self.largest_service_demand.clone();
        let Z = self.think_time.clone();
        let lower = T::max(D.clone(), N.clone() * Dmax - Z);
        let upper = N * D;
        (lower, upper).into()
    }

    /// Calculate balanced system bound for throughput
    ///
    /// # Arguments
    ///
    /// `requests` - Number of requests
    #[allow(non_snake_case)]
    pub fn throughput_balanced_system_bound(&self, requests: T) -> Bounds<T> {
        let N = requests;
        let D = self.sum_of_service_demands.clone();
        let Dmax = self.largest_service_demand.clone();
        let Dave = self.average_service_demand.clone();
        let Z = self.think_time.clone();
        let lower = N.clone()
            / (D.clone()
                + Z.clone()
                + (N.clone() - T::one()) * Dmax.clone()
                    / (T::one() + Z.clone() / (N.clone() * D.clone())));
        let upper = T::min(
            T::one() / Dmax,
            N.clone() / (D.clone() + Z.clone() + (N - T::one()) * Dave / (T::one() + Z / D)),
        );
        (lower, upper).into()
    }

    /// Calculate balanced system bound for response time
    ///
    /// # Arguments
    ///
    /// `requests` - Number of requests
    #[allow(non_snake_case)]
    pub fn response_balanced_system_bound(&self, requests: T) -> Bounds<T> {
        let N = requests;
        let D = self.sum_of_service_demands.clone();
        let Dmax = self.largest_service_demand.clone();
        let Dave = self.average_service_demand.clone();
        let Z = self.think_time.clone();
        let lower = T::max(
            N.clone() * Dmax.clone() - Z.clone(),
            D.clone() + (N.clone() - T::one()) * Dave / (T::one() + Z.clone() / D.clone()),
        );
        let upper = D.clone() + (N.clone() - T::one()) * Dmax / (T::one() + Z / (N * D));
        (lower, upper).into()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[allow(clippy::float_cmp)]
    #[test]
    fn bounds() {
        let b1 = Bounds::from((2.3, 4.6));
        assert_eq!(2.3, b1.lower());
        let b2: Bounds<i32> = (1, 3).into();
        assert_eq!(3, b2.upper());
    }
}
