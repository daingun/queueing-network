//! # Closed Model Solution
//!
//! _E. D. Lazowska, J. Zahorjan, G. S. Graham, K. C. Sevcik,
//! Quantitative System Performance, Prentice-Hall Inc., 1984_
//!
//! Closed models refer to terminal or batch workloads and it is known as
//! _mean value analysis (MVA)_.
//!
//! The solution returns:
//! * the throughput of the system
//! * the response time of the system
//! * the average number in system
//! * the queue length at each centre
//! * the response time of each centre
//! * the utilization of each centre
//!
//! # Exact solution technique
//!
//! The exact solution is based on the fact that the queue length seen at arrival
//! to a queue when there are _N_ cusomers in the network is equal to the time
//! averaged queue length there with one less customer in the network.
//!
//! The exact solution has a _O(NK)_ time complexity, where _N_ is the number
//! of customers in the network and _K_ is the number of service centres.
//!
//! # Approximate solution technique
//!
//! The approximate solution estimates the arrival instant queue length as
//! `Ak(N) = (N-1)/N * Qk(N)`.
//!
//! The approximate solution has a _O(IK)_ time complexity, where _K_ is the
//! number of service centres and _I_ the number of iteration for convergence.
//!
//! # Examples
//!
//! ```
//! use reti_di_code::single_class::closed_model::{ClosedModel, ServiceCentre};
//! let sc1 = ServiceCentre::new_queueing_centre("cpu", 0.605_f32);
//! let sc2 = ServiceCentre::new_queueing_centre("disk1", 2.1);
//! let sc3 = ServiceCentre::new_queueing_centre("disk2", 1.35);
//!
//! let closed_model = ClosedModel::new(&[sc1, sc2, sc3], 15., 3_u16);
//! let s = closed_model.solution().unwrap();
//!
//! // System output.
//! let thr = s.throughput();
//! let rt = s.response_time();
//! let q = s.queue();
//! // Single centre output.
//! let cpu_utilization = s.centre_utilization(&"cpu").unwrap();
//! let cpu_queue = s.centre_queue(&"cpu").unwrap();
//! let cpu_response_time = s.centre_response_time(&"cpu").unwrap();
//! ```
//!

use std::{
    borrow::Borrow,
    collections::{
        hash_map::{Values, ValuesMut},
        HashMap,
    },
    hash::Hash,
    ops::{Neg, RangeInclusive},
};

use crate::{error::Error, One, Ops, ServiceKind, TryFromInt, Zero};

/// Struct holding the data of a single service centre
#[derive(Clone, Debug)]
pub struct ServiceCentre<I, T> {
    /// Identification
    id: I,
    /// Service centre kind
    kind: ServiceKind,
    /// Service centre data
    data: CentreData<T>,
}

impl<I, T> ServiceCentre<I, T>
where
    I: Clone,
{
    /// Get the identification of the service centre
    fn id(&self) -> I {
        self.id.clone()
    }
}

impl<I, T> ServiceCentre<I, T>
where
    T: Clone,
{
    /// Get the demand of the service centre
    fn demand(&self) -> T {
        self.data.demand.clone()
    }

    /// Get the queue of the service centre
    fn queue(&self) -> T {
        self.data.queue.clone()
    }

    /// Get the response time of the service centre
    fn response_time(&self) -> T {
        self.data.response_time.clone()
    }
}

impl<I, T> ServiceCentre<I, T>
where
    T: Clone + Zero,
{
    /// Create a delay service centre
    ///
    /// # Arguments
    ///
    /// * `id` - Identification of the service centre
    /// * `demand` - Service centre demand
    pub fn new_delay_centre(id: I, demand: T) -> Self {
        Self {
            id,
            kind: ServiceKind::DelayCentre,
            data: CentreData::new(demand),
        }
    }

    /// Create a queueing service centre
    ///
    /// # Arguments
    ///
    /// * `id` - Identification of the service centre
    /// * `demand` - Service centre demand
    pub fn new_queueing_centre(id: I, demand: T) -> Self {
        Self {
            id,
            kind: ServiceKind::QueueingCentre,
            data: CentreData::new(demand),
        }
    }
}

impl<I, T> ServiceCentre<I, T>
where
    T: Clone + One + Ops,
{
    /// Update the response time of the service centre
    fn update_response_time(&mut self) {
        self.data.response_time = match self.kind {
            ServiceKind::DelayCentre => self.data.demand.clone(),
            ServiceKind::QueueingCentre => {
                self.data.demand.clone() * (T::one() + self.data.queue.clone())
            }
        }
    }
}

/// Data of each service centre
#[derive(Clone, Debug)]
struct CentreData<T> {
    /// Service centre demand
    demand: T,
    /// Service centre queue
    queue: T,
    /// Service centre response time
    response_time: T,
}

impl<T> CentreData<T>
where
    T: Zero,
{
    /// Create a new `CentreData` given a demand
    fn new(demand: T) -> Self {
        Self {
            demand,
            queue: T::zero(),
            response_time: T::zero(),
        }
    }
}

/// Collection key-value of the service centres
#[derive(Clone, Debug)]
struct System<I, T> {
    centres: HashMap<I, ServiceCentre<I, T>>,
}

impl<I, T> System<I, T> {
    /// Returns the iterator over the data of the service centres.
    fn values(&self) -> Values<I, ServiceCentre<I, T>> {
        self.centres.values()
    }

    /// Returns the mutable iterator over the data of the service centres.
    fn values_mut(&mut self) -> ValuesMut<I, ServiceCentre<I, T>> {
        self.centres.values_mut()
    }
}

impl<I, T> System<I, T>
where
    T: TryFromInt<usize>,
{
    /// Get the number of centres in the system
    fn number_of_centres(&self) -> Result<T, Error> {
        T::try_from(self.centres.len()).map_err(Error::conversion_error)
    }
}

impl<I, T> System<I, T>
where
    T: Clone + One + Ops,
{
    /// Update the response time of the centres of the system.
    fn update_response_times(&mut self) {
        for sc in self.values_mut() {
            sc.update_response_time();
        }
    }
}

impl<I, T> System<I, T>
where
    T: Clone + Ops,
{
    /// Update the queue of the centres of the system.
    ///
    /// # Arguments
    ///
    /// * `throughput` - Throughput of the system
    fn update_queues(&mut self, throughput: T) {
        for sc in self.values_mut() {
            sc.data.queue = throughput.clone() * sc.response_time();
        }
    }
}

impl<K, T> System<K, T>
where
    K: Eq + Hash,
{
    /// Returns a reference to the service centre corresponding to the key
    ///
    /// # Arguments
    ///
    /// * `k` - Identification of the service centre
    fn get<Q: ?Sized>(&self, k: &Q) -> Option<&ServiceCentre<K, T>>
    where
        K: Borrow<Q>,
        Q: Hash + Eq,
    {
        self.centres.get(k)
    }
}

/// Input data for the close model
#[derive(Clone, Debug)]
pub struct ClosedModel<I, T, U> {
    /// Collection of service centres
    service_centres: System<I, T>,
    /// Think time of the system
    think_time: T,
    /// Population of the system
    population: U,
}

impl<I, T, U> ClosedModel<I, T, U>
where
    I: Clone + Eq + Hash,
    T: Clone + TryFromInt<U> + One + Ops + Zero,
    U: Clone + One,
    RangeInclusive<U>: IntoIterator<Item = U>,
{
    /// Create a new closed model
    ///
    /// # Arguments
    ///
    /// * `service_centres` - Collection of service centres
    /// * `think_time` - Think time of the terminals
    /// * `population` - Number of customers in the system
    pub fn new(service_centres: &[ServiceCentre<I, T>], think_time: T, population: U) -> Self {
        Self {
            service_centres: System {
                centres: service_centres
                    .iter()
                    .map(|sc| (sc.id(), sc.clone()))
                    .collect(),
            },
            think_time,
            population,
        }
    }

    /// Get the population of the model
    fn population(&self) -> U {
        self.population.clone()
    }

    /// Get the think time of the model
    fn think_time(&self) -> T {
        self.think_time.clone()
    }

    /// Compute the solution for a closed model
    ///
    /// # Errors
    ///
    /// Returns an error if the population type cannot be converted into `T`
    #[allow(non_snake_case)]
    pub fn solution(&self) -> Result<Solution<I, T>, Error> {
        let mut cms = Solution::start(self)?;

        for n in U::one()..=self.population() {
            cms.service_centres.update_response_times();
            let R: T = cms
                .service_centres
                .values()
                .fold(T::zero(), |acc, sc| acc + sc.response_time());
            cms.throughput =
                T::try_from(n).map_err(Error::conversion_error)? / (self.think_time() + R);
            cms.service_centres.update_queues(cms.throughput.clone());
        }
        Ok(cms)
    }

    /// Compute the approximate solution for a closed model
    ///
    /// # Arguments
    ///
    /// * `abs` - absolute error to stop the iterations
    ///
    /// # Errors
    ///
    /// Returns an error if the population type cannot be converted into `T`,
    /// or the size of the service centres cannot be expressed as `T`
    #[allow(non_snake_case)]
    pub fn approximate_solution(&self, abs: T) -> Result<Solution<I, T>, Error>
    where
        T: Neg<Output = T> + PartialOrd + TryFromInt<usize>,
    {
        let mut cms = Solution::start(self)?;
        let pop = cms.population();

        // Ak(N): average number of customers seen at center k when a new customer arrives
        let mut A = HashMap::new();
        let N = cms.population();
        let K = cms.service_centres.number_of_centres()?;
        let queue_length = N / K;
        for sc in cms.service_centres.values_mut() {
            sc.data.queue = queue_length.clone();
            A.insert(sc.id(), (pop.clone() - T::one()) / pop.clone() * sc.queue());
        }

        loop {
            for sc in cms.service_centres.values_mut() {
                let a = A.get(&sc.id).ok_or(Error::missing_service_centre_error())?;
                sc.data.response_time = match sc.kind {
                    ServiceKind::DelayCentre => sc.demand(),
                    ServiceKind::QueueingCentre => sc.demand() * (T::one() + a.clone()),
                }
            }
            let R: T = cms
                .service_centres
                .values()
                .fold(T::zero(), |acc, sc| acc + sc.response_time());
            cms.throughput = cms.population() / (self.think_time() + R);

            let mut tmp_queue = HashMap::new();
            for sc in cms.service_centres.values() {
                tmp_queue.insert(sc.id(), cms.throughput() * sc.response_time());
            }

            let stop = cms.service_centres.values().all(|sc| {
                let diff = sc.queue() - tmp_queue.get(&sc.id).unwrap().clone();
                diff < abs && -diff < abs
            });

            for sc in cms.service_centres.values_mut() {
                sc.data.queue = tmp_queue
                    .get(&sc.id)
                    .ok_or(Error::missing_service_centre_error())?
                    .clone();
            }

            if stop {
                break;
            }

            for sc in cms.service_centres.values() {
                A.entry(sc.id())
                    .and_modify(|a| *a = (pop.clone() - T::one()) / pop.clone() * sc.queue());
            }
        }
        Ok(cms)
    }
}

/// Struct holding the solution of a closed model
#[derive(Clone, Debug)]
pub struct Solution<I, T> {
    /// Service centres of the system
    service_centres: System<I, T>,
    /// Throughput of the entire system
    throughput: T,
    /// Think time of the system
    think_time: T,
    /// Population of the system
    population: T,
}

impl<I, T> Solution<I, T>
where
    I: Eq + Hash,
    T: Clone + Ops,
{
    /// Create the starting step of the solution
    ///
    /// # Arguments
    ///
    /// * `model` - Closed model structure
    ///
    /// # Errors
    ///
    /// Returns an error if the population type cannot be converted into `T`
    fn start<U>(model: &ClosedModel<I, T, U>) -> Result<Self, Error>
    where
        I: Clone,
        T: TryFromInt<U> + One + Zero,
        U: Clone + One,
        RangeInclusive<U>: IntoIterator<Item = U>,
    {
        Ok(Solution {
            service_centres: model.service_centres.clone(),
            throughput: T::zero(),
            think_time: model.think_time(),
            population: T::try_from(model.population()).map_err(Error::conversion_error)?,
        })
    }

    /// Get the population of the entire system
    fn population(&self) -> T {
        self.population.clone()
    }

    /// Get the think time of the entire system
    fn think_time(&self) -> T {
        self.think_time.clone()
    }

    /// Get the throughput of the entire system
    pub fn throughput(&self) -> T {
        self.throughput.clone()
    }

    /// Get the response time of the entire system
    pub fn response_time(&self) -> T {
        crate::laws::response_time_law(self.population(), self.throughput(), self.think_time())
    }

    /// Get the average number of requests in the entire system
    pub fn queue(&self) -> T {
        self.population() - self.throughput() * self.think_time()
    }

    /// Get the number of requests in queue at the given service centre
    ///
    /// # Arguments
    ///
    /// * `centre` - Service centre index
    pub fn centre_queue<Q: ?Sized>(&self, centre: &Q) -> Option<T>
    where
        I: Borrow<Q>,
        Q: Hash + Eq,
    {
        self.service_centres.get(centre).map(ServiceCentre::queue)
    }

    /// Get the response time of the given service centre
    ///
    /// # Arguments
    ///
    /// * `centre` - Service centre index
    pub fn centre_response_time<Q: ?Sized>(&self, centre: &Q) -> Option<T>
    where
        I: Borrow<Q>,
        Q: Hash + Eq,
    {
        self.service_centres
            .get(centre)
            .map(ServiceCentre::response_time)
    }

    /// Get the utilization of the given service centre
    ///
    /// # Arguments
    ///
    /// * `centre` - Service centre index
    pub fn centre_utilization<Q: ?Sized>(&self, centre: &Q) -> Option<T>
    where
        I: Borrow<Q>,
        Q: Hash + Eq,
    {
        self.service_centres
            .get(centre)
            .map(|sc| self.throughput() * sc.demand())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn closed_model() {
        let sc1 = ServiceCentre::new_queueing_centre(1, 0.605_f32);
        let sc2 = ServiceCentre::new_queueing_centre(2, 2.1);
        let sc3 = ServiceCentre::new_queueing_centre(3, 1.35);
        let cs = ClosedModel::new(&[sc1, sc2, sc3], 15., 3_u16);
        let s = cs.solution().unwrap();

        assert_eq!("0.152", format!("{:.3}", s.throughput()));
        assert_eq!("4.80", format!("{:.2}", s.response_time()));
        assert_eq!("0.73", format!("{:.2}", s.queue()));

        assert_eq!("0.092", format!("{:.3}", s.centre_utilization(&1).unwrap()));
        assert_eq!("0.098", format!("{:.3}", s.centre_queue(&1).unwrap()));
        assert_eq!(
            "0.64",
            format!("{:.2}", s.centre_response_time(&1).unwrap())
        );
    }

    #[test]
    fn approximate_closed_model() {
        let sc1 = ServiceCentre::new_queueing_centre(1, 0.605_f32);
        let sc2 = ServiceCentre::new_queueing_centre(2, 2.1);
        let sc3 = ServiceCentre::new_queueing_centre(3, 1.35);
        let cs = ClosedModel::new(&[sc1, sc2, sc3], 15., 3_u16);
        let s = cs.approximate_solution(0.001).unwrap();

        assert_eq!("0.1510", format!("{:.4}", s.throughput()));
        assert_eq!("4.8700", format!("{:.4}", s.response_time()));

        assert_eq!("0.0973", format!("{:.4}", s.centre_queue(&1).unwrap()));
        assert_eq!("0.4021", format!("{:.4}", s.centre_queue(&2).unwrap()));
        assert_eq!("0.2359", format!("{:.4}", s.centre_queue(&3).unwrap()));
    }
}
