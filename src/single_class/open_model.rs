//! # Open Model Solution
//!
//! _E. D. Lazowska, J. Zahorjan, G. S. Graham, K. C. Sevcik,
//! Quantitative System Performance, Prentice-Hall Inc., 1984_
//!
//! Open models refer to transaction workloads and the system throughput
//! is given as an input.
//!
//! The solution returns:
//! * the processing capacity of the system
//! * the throughput of the system
//! * the response time of the system
//! * the average number in system
//! * the queue length at each centre
//! * the response time of each centre
//! * the utilization of each centre
//!
//! # Examples
//!
//! ```
//! use reti_di_code::single_class::open_model::{OpenModel, ServiceCentre};
//! let sc1 = ServiceCentre::new_queueing_centre("cpu", 0.605_f32);
//! let sc2 = ServiceCentre::new_queueing_centre("disk1", 2.1);
//! let sc3 = ServiceCentre::new_queueing_centre("disk2", 1.35);
//!
//! let open_model = OpenModel::new(&[sc1, sc2, sc3]);
//! let s = open_model.solution(0.3);
//!
//! // System output.
//! let pc = s.processing_capacity();
//! let rt = s.response_time();
//! let q = s.queue();
//! // Single centre output.
//! let cpu_utilization = s.centre_utilization(&"cpu").unwrap();
//! let cpu_queue = s.centre_queue(&"cpu").unwrap();
//! let cpu_response_time = s.centre_response_time(&"cpu").unwrap();
//! ```
//!

use std::{
    borrow::Borrow,
    collections::{
        hash_map::{Values, ValuesMut},
        HashMap,
    },
    hash::Hash,
};

use crate::{One, Ops, ServiceKind, Zero};

/// Calculate the largest service demands given an iterator
///
/// # Arguments
///
/// * `demands` - Iterator of service centre demands
fn largest_service_demand_iter<II, T>(demands: II) -> T
where
    II: IntoIterator<Item = T>,
    T: PartialOrd,
{
    demands
        .into_iter()
        .max_by(|x, y| x.partial_cmp(y).unwrap())
        .unwrap()
}

/// Struct holding the data of a single service centre
#[derive(Clone, Debug)]
pub struct ServiceCentre<I, T> {
    /// Identification
    id: I,
    /// Service centre kind
    kind: ServiceKind,
    /// Service centre data
    data: CentreData<T>,
}

impl<I, T> ServiceCentre<I, T>
where
    I: Clone,
{
    /// Get the identification of the service centre
    fn id(&self) -> I {
        self.id.clone()
    }
}

impl<I, T> ServiceCentre<I, T>
where
    T: Clone,
{
    /// Get the service demand of the service centre
    fn demand(&self) -> T {
        self.data.demand.clone()
    }

    /// Get the utilization of the service centre
    fn utilization(&self) -> T {
        self.data.utilization.clone()
    }

    /// Get the response time of the service centre
    fn response_time(&self) -> T {
        self.data.response_time.clone()
    }

    /// Get the queue of the service centre
    fn queue(&self) -> T {
        self.data.queue.clone()
    }
}

impl<I, T> ServiceCentre<I, T>
where
    T: Zero,
{
    /// Create a new delay service centre
    ///
    /// # Arguments
    ///
    /// * `id` - Identification of the service centre
    /// * `demand` - Demand of the service centre
    pub fn new_delay_centre(id: I, demand: T) -> Self {
        ServiceCentre {
            id,
            kind: ServiceKind::DelayCentre,
            data: CentreData::new(demand, T::zero(), T::zero(), T::zero()),
        }
    }

    /// Create a new queueing service centre
    ///
    /// # Arguments
    ///
    /// * `id` - Identification of the service centre
    /// * `demand` - Demand of the service centre
    pub fn new_queueing_centre(id: I, demand: T) -> Self {
        ServiceCentre {
            id,
            kind: ServiceKind::QueueingCentre,
            data: CentreData::new(demand, T::zero(), T::zero(), T::zero()),
        }
    }
}

/// Service centre data
#[derive(Clone, Debug)]
struct CentreData<T> {
    /// Service demand
    demand: T,
    /// Service centre utilization
    utilization: T,
    /// Service centre response time
    response_time: T,
    /// Service centre queue
    queue: T,
}

impl<T> CentreData<T> {
    /// Create the `CentreData` struct
    ///
    /// # Arguments
    ///
    /// * `demand` - Service demand
    /// * `utilization` - Service centre utilization
    /// * `response_time` - Service centre response time
    /// * `queue` - Service centre queue
    fn new(demand: T, utilization: T, response_time: T, queue: T) -> Self {
        Self {
            demand,
            utilization,
            response_time,
            queue,
        }
    }
}

/// Open model
#[derive(Clone, Debug)]
pub struct OpenModel<I, T> {
    /// Service centre collection
    service_centres: System<I, T>,
    /// Largest service demand of service centres
    largest_service_demand: T,
}

impl<I, T> OpenModel<I, T>
where
    T: Clone,
{
    /// Get the largest service demand
    fn largest_service_demand(&self) -> T {
        self.largest_service_demand.clone()
    }
}

impl<I, T> OpenModel<I, T>
where
    I: Clone + Eq + Hash,
    T: Clone + PartialOrd,
{
    /// Create an open model system
    ///
    /// # Arguments
    ///
    /// * `centres` - Service centres
    pub fn new(centres: &[ServiceCentre<I, T>]) -> Self {
        Self {
            service_centres: System {
                centres: centres.iter().map(|sc| (sc.id(), sc.clone())).collect(),
            },
            largest_service_demand: largest_service_demand_iter(
                centres.iter().map(ServiceCentre::demand),
            ),
        }
    }
}

impl<I, T> OpenModel<I, T>
where
    I: Clone + Eq + Hash,
    T: Clone + One + Ops + Zero,
{
    /// Calculate the solution for the open model
    ///
    /// # Arguments
    ///
    /// * `arrival_rate` - Arrival rate of transactions
    pub fn solution(&self, arrival_rate: T) -> Solution<I, T> {
        let mut service_centres = self.service_centres.clone();
        for sc in service_centres.values_mut() {
            let u = arrival_rate.clone() * sc.demand();
            let r = match sc.kind {
                ServiceKind::DelayCentre => sc.demand(),
                ServiceKind::QueueingCentre => sc.demand() / (T::one() - u.clone()),
            };
            let q = arrival_rate.clone() * r.clone();
            sc.data.utilization = u;
            sc.data.response_time = r;
            sc.data.queue = q;
        }
        let system_response_time: T = service_centres
            .values()
            .map(ServiceCentre::response_time)
            .fold(T::zero(), |acc, rt| acc + rt);
        let average_number_in_system: T = arrival_rate.clone() * system_response_time.clone();
        Solution {
            processing_capacity: T::one() / self.largest_service_demand(),
            throughput: arrival_rate,
            service_centres,
            system_response_time,
            average_number_in_system,
        }
    }
}

/// Collection key-value of the service centres
#[derive(Clone, Debug)]
struct System<K, T> {
    centres: HashMap<K, ServiceCentre<K, T>>,
}

impl<K, T> System<K, T> {
    /// Returns the iterator over the data of the service centres.
    fn values(&self) -> Values<K, ServiceCentre<K, T>> {
        self.centres.values()
    }

    /// Returns the mutable iterator over the data of the service centres.
    fn values_mut(&mut self) -> ValuesMut<K, ServiceCentre<K, T>> {
        self.centres.values_mut()
    }
}

impl<K, T> System<K, T>
where
    K: Eq + Hash,
{
    /// Returns a reference to the value corresponding to the key
    fn get<Q: ?Sized>(&self, k: &Q) -> Option<&ServiceCentre<K, T>>
    where
        K: Borrow<Q>,
        Q: Hash + Eq,
    {
        self.centres.get(k)
    }
}

/// Structure holding data for the solution of an open model
#[derive(Clone, Debug)]
pub struct Solution<I: Eq + Hash, T> {
    /// Processing capacity of the entire system
    processing_capacity: T,
    /// Throughput of the entire system
    throughput: T,
    /// Service centres informations
    service_centres: System<I, T>,
    /// Response time of the entire system
    system_response_time: T,
    /// Average number of customers in the system
    average_number_in_system: T,
}

impl<I, T> Solution<I, T>
where
    I: Eq + Hash,
    T: Clone,
{
    /// Get the processing capacity of the entire system
    pub fn processing_capacity(&self) -> T {
        self.processing_capacity.clone()
    }

    /// Get the throughput of the entire system
    pub fn throughput(&self) -> T {
        self.throughput.clone()
    }

    /// Get the response time of the entire system
    pub fn response_time(&self) -> T {
        self.system_response_time.clone()
    }

    /// Get the average number in the entire system
    pub fn queue(&self) -> T {
        self.average_number_in_system.clone()
    }

    /// Get the number of requests in queue at the given service centre
    ///
    /// # Arguments
    ///
    /// * `centre` - Service centre index
    pub fn centre_queue<Q: ?Sized>(&self, centre: &Q) -> Option<T>
    where
        I: Borrow<Q>,
        Q: Hash + Eq,
    {
        self.service_centres.get(centre).map(ServiceCentre::queue)
    }

    /// Get the response time of the given service centre
    ///
    /// # Arguments
    ///
    /// * `centre` - Service centre index
    pub fn centre_response_time<Q: ?Sized>(&self, centre: &Q) -> Option<T>
    where
        I: Borrow<Q>,
        Q: Hash + Eq,
    {
        self.service_centres
            .get(centre)
            .map(ServiceCentre::response_time)
    }

    /// Get the utilization of the given service centre
    ///
    /// # Arguments
    ///
    /// * `centre` - Service centre index
    pub fn centre_utilization<Q: ?Sized>(&self, centre: &Q) -> Option<T>
    where
        I: Borrow<Q>,
        Q: Hash + Eq,
    {
        self.service_centres
            .get(centre)
            .map(ServiceCentre::utilization)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn open_model() {
        let sc1 = ServiceCentre::new_queueing_centre(1, 0.605_f32);
        let sc2 = ServiceCentre::new_queueing_centre(2, 2.1);
        let sc3 = ServiceCentre::new_queueing_centre(3, 1.35);

        let om = OpenModel::new(&[sc1, sc2, sc3]);
        let s = om.solution(0.3);
        assert_eq!("0.476", format!("{:.3}", s.processing_capacity()));
        assert_eq!("8.684", format!("{:.3}", s.response_time()));
        assert_eq!("2.605", format!("{:.3}", s.queue()));

        assert_eq!("0.182", format!("{:.3}", s.centre_utilization(&1).unwrap()));
        assert_eq!("0.222", format!("{:.3}", s.centre_queue(&1).unwrap()));
        assert_eq!(
            "0.739",
            format!("{:.3}", s.centre_response_time(&1).unwrap())
        );
    }

    #[test]
    fn open_model_with_delay() {
        let sc1 = ServiceCentre::new_delay_centre(1, 0.605_f32);
        let sc2 = ServiceCentre::new_queueing_centre(2, 2.1);
        let sc3 = ServiceCentre::new_queueing_centre(3, 1.35);

        let om = OpenModel::new(&[sc1, sc2, sc3]);
        let s = om.solution(0.3);
        assert_eq!("0.476", format!("{:.3}", s.processing_capacity()));
        assert_eq!("8.550", format!("{:.3}", s.response_time()));
        assert_eq!("2.565", format!("{:.3}", s.queue()));
        assert_eq!("0.300", format!("{:.3}", s.throughput()));

        assert_eq!("0.182", format!("{:.3}", s.centre_utilization(&1).unwrap()));
        assert_eq!("0.182", format!("{:.3}", s.centre_queue(&1).unwrap()));
        assert_eq!(
            "0.605",
            format!("{:.3}", s.centre_response_time(&1).unwrap())
        );
    }
}
