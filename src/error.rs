//! Error representation

use crate::TryFromIntError;

use std::{error, fmt};

///  Custom `Err` variant of `std::Result`
pub struct Error {
    /// Internal representation
    repr: Repr,
}

/// Internal representation of `Error`
#[derive(Debug)]
#[allow(dead_code)]
enum Repr {
    /// Conversion error
    Conversion(TryFromIntError),
    /// Missing class
    MissingClass,
    /// Missing service centre
    MissingServiceCentre,
    /// Generic error
    Generic(&'static str),
}

/// Kind of `Error`
#[allow(clippy::module_name_repetitions)]
#[derive(Clone, Copy, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub enum ErrorKind {
    /// Conversion error
    Conversion,
    /// Missing class
    MissingClass,
    /// Missing service centre
    MissingServiceCentre,
    /// Generic error
    Generic,
}

impl Error {
    /// Create an `Error` of kind `Conversion`
    pub(crate) fn conversion_error(err: TryFromIntError) -> Self {
        Error {
            repr: Repr::Conversion(err),
        }
    }

    /// Create an `Error` of kind `MissingClass`
    pub(crate) fn missing_class_error() -> Self {
        Error {
            repr: Repr::MissingClass,
        }
    }
    /// Create an `Error` of kind `MissingServiceCentre`
    pub(crate) fn missing_service_centre_error() -> Self {
        Error {
            repr: Repr::MissingServiceCentre,
        }
    }

    /// Create an `Error` of kind `Generic`
    ///
    /// # Arguments
    ///
    /// * `s` - error string
    #[allow(dead_code)]
    pub(crate) fn generic_error(s: &'static str) -> Self {
        Error {
            repr: Repr::Generic(s),
        }
    }

    /// Returns the representation of the kind of error
    #[must_use]
    pub fn kind(&self) -> ErrorKind {
        match self.repr {
            Repr::Conversion(_) => ErrorKind::Conversion,
            Repr::MissingClass => ErrorKind::MissingClass,
            Repr::MissingServiceCentre => ErrorKind::MissingServiceCentre,
            Repr::Generic(_) => ErrorKind::Generic,
        }
    }
}

impl error::Error for Error {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        match self.repr {
            Repr::Conversion(ref e) => Some(e),
            _ => None,
        }
    }
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self.repr {
            Repr::Conversion(_) => {
                write!(f, "Conversion error")
            }
            Repr::MissingClass => {
                write!(f, "Missing class error")
            }
            Repr::MissingServiceCentre => {
                write!(f, "Missing service centre error")
            }
            Repr::Generic(s) => write!(f, "{s}"),
        }
    }
}

impl fmt::Debug for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        fmt::Debug::fmt(&self.repr, f)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    use error::Error as Errr;

    #[test]
    fn conversion_error() {
        let e = Error::conversion_error(TryFromIntError(()));
        assert_eq!(ErrorKind::Conversion, e.kind());
        assert!(e.source().is_some());
        assert_eq!("Conversion error", e.to_string());
        assert_eq!("Conversion(TryFromIntError(()))", format!("{e:?}"));
    }

    #[test]
    fn missing_class_error() {
        let e = Error::missing_class_error();
        assert_eq!(ErrorKind::MissingClass, e.kind());
        assert!(e.source().is_none());
        assert_eq!("Missing class error", e.to_string());
        assert_eq!("MissingClass", format!("{e:?}"));
    }

    #[test]
    fn missing_service_centre_error() {
        let e = Error::missing_service_centre_error();
        assert_eq!(ErrorKind::MissingServiceCentre, e.kind());
        assert!(e.source().is_none());
        assert_eq!("Missing service centre error", e.to_string());
        assert_eq!("MissingServiceCentre", format!("{e:?}"));
    }

    #[test]
    fn generic_error() {
        let e = Error::generic_error("my error");
        assert_eq!(ErrorKind::Generic, e.kind());
        assert!(e.source().is_none());
        assert_eq!("my error", e.to_string());
        assert_eq!("Generic(\"my error\")", format!("{e:?}"));
    }
}
