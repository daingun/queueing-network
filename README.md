# Queueing Network Models

This crate allows the modelling of separable queueing networks.

Three workloads are defined:
  * transaction
  * batch
  * terminal

The first one is for open models while the other two are used in closed models

## Asymptotic bounds

Asymptotic bounds

## Balanced system bounds

Balanced system bounds

## Single class models

Single class models

## Multiple classes models

Multiple classes models

## References

_E. D. Lazowska, J. Zahorjan, G. S. Graham, K. C. Sevcik,
Quantitative System Performance, Prentice-Hall Inc., 1984_
